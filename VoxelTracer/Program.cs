﻿using System;

namespace VoxelTracer
{
	class Program
	{
		static void Main(string[] args)
		{
			MainLoop.Instance = new MainLoop();
			MainLoop.Instance.Start();
		}
	}
}
