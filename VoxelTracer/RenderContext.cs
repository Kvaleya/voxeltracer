﻿using System;
using System.Collections.Generic;
using System.Text;
using GlobCore;
using OpenTK.Mathematics;

namespace VoxelTracer
{
	class RenderContext
	{
		public int DisplayWidth = 1, DisplayHeight = 1, RenderWidth = 1, RenderHeight = 1;
		public Device Device;
		public double Ellapsed = 0;
		public double DeltaT = 0;
		public double RealDeltaT = 0;
		public double NoiseEllapsed => Ellapsed - Math.Floor(Ellapsed);

		public double SunAngle = 1;

		public Vector3 SunLight = Vector3.One;

		public int LevelIndex = 0;

		public Vector3 SunDir
		{
			get
			{
				return Vector3.Transform(new Vector3(1, 0, 1).Normalized(), Quaternion.FromAxisAngle(new Vector3(-1, 3, 1).Normalized(), (float)SunAngle));
				Vector3 d = new Vector3((float)Math.Sin(SunAngle), 0.5f, (float)Math.Cos(SunAngle));
				d.Normalize();
				return d;
			}
		}

		public void SetSceneUniforms(Shader s)
		{
			s.SetUniformF("lightDir", SunDir);
			s.SetUniformF("lightColor", SunLight);
		}
	}
}
