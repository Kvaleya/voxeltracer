﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using OpenTK;
using OpenTK.Mathematics;
using OpenTK.Graphics.OpenGL;
using GlobCore;
using VoxelTracer.Rendering;

namespace VoxelTracer
{
	class WorldRenderer
	{
		public Device Device { get { return Context.Device; } }
		public RenderContext Context;

		GraphicsPipeline _psoTonemap;
		GraphicsPipeline _psoTonemapOff;

		ComputePipeline _psoLightShadow;
		
		ComputePipeline _psoExposure;

		ComputePipeline _psoCompose;

		FrameBuffer _fboTonemap;

		GraphicsPipeline _psoClear;

		Texture2D _texShadowRaw;
		Texture2D _texSceneTonemapped;
		Texture2D _texSceneComposed;		

		Texture2D[] _texBlueNoiseArray;

		TextureCube _texSky;

		bool _resize = true;

		Camera _cameraHistory = null;

		int HistogramStops => _psoExposure.ShaderCompute.WorkGroupSizeX;
		const float HistogramStart = -12;
		const float HistogramRange = 32;

		int _bufferHistogramZeroes = 0;
		int _bufferHistogram = 0;
		int _bufferExposureA = 0;
		int _bufferExposureB = 0;

		ModuleSmaa _moduleSmaa;
		ModuleWorld _moduleWorld;
		ModuleGbuffer _moduleGbuffer;
		ModuleDiffuse _moduleDiffuse;
		ModuleSpecular _moduleSpecular;
		ModuleAtmosphere _moduleAtmosphere;
		ModuleUpscaler _moduleUpscaler;

		int _qualityPreset = 3;

		public int QualityPreset
		{
			get { return _qualityPreset; }
			set
			{
				_qualityPreset = value % _qualityPresets.Length;
				_resize = true;
				var p = _qualityPresets[_qualityPreset];
				Console.WriteLine("Quality preset " + (_qualityPreset + 1) + " / " + _qualityPresets.Length + 
					$" Resshifts: GI: {p[0]} Refl trace: {p[1]} Refl resolve: {p[2]}");
			}
		}

		static readonly int[][] _qualityPresets =
		{
			// diffuse resolution shift, specular trace rshift, specular resolve rshift
			new int[] { 3, 1, 1 },
			new int[] { 2, 1, 1 },
			new int[] { 2, 0, 0 },
			new int[] { 1, 0, 0 }, // Ultra good
		};

		public WorldRenderer(Device device)
		{
			Context = new RenderContext();
			Context.Device = device;

			_texBlueNoiseArray = LoadBlueNoise();

			Device.Update();

			_moduleSmaa = new ModuleSmaa(Context);
			_moduleWorld = new ModuleWorld(Context);
			_moduleGbuffer = new ModuleGbuffer(Context);
			_moduleDiffuse = new ModuleDiffuse(Context);
			_moduleSpecular = new ModuleSpecular(Context);
			_moduleAtmosphere = new ModuleAtmosphere(Context);
			_moduleUpscaler = new ModuleUpscaler(Context);

			_psoExposure = new ComputePipeline(Device, Device.GetShader("autoExposure.comp"));

			_psoClear = new GraphicsPipeline(Device, null, null,
				null, new RasterizerState(), new DepthState(DepthFunction.Always, true));

			const int skyRes = 128;
			_texSky = new TextureCube(Device, "Sky", SizedInternalFormatGlob.RGBA16F, skyRes, skyRes, 0);

			uint[] zeroes = new uint[HistogramStops];
			_bufferHistogramZeroes = GlobCore.Utils.CreateBuffer(BufferTarget.ShaderStorageBuffer, new IntPtr(HistogramStops * 4), zeroes, BufferStorageFlags.None, "Histogram zeroes");
			_bufferHistogram = GlobCore.Utils.CreateBuffer(BufferTarget.ShaderStorageBuffer, new IntPtr(HistogramStops * 4), IntPtr.Zero, BufferStorageFlags.None, "Histogram");
			_bufferExposureA = GlobCore.Utils.CreateBuffer(BufferTarget.ShaderStorageBuffer, new IntPtr(16), new float[] { 0f, 0f, 0f, 0f }, BufferStorageFlags.None, "Exposure A");
			_bufferExposureB = GlobCore.Utils.CreateBuffer(BufferTarget.ShaderStorageBuffer, new IntPtr(16), new float[] { 0f, 0f, 0f, 0f }, BufferStorageFlags.None, "Exposure B");

			//
			// Generic stuff
			//
			_psoTonemap = new GraphicsPipeline(Device,
				Device.GetShader("fullscreenSimple.vert"),
				Device.GetShader("tonemap.frag"),
				null, new RasterizerState(), new DepthState(DepthFunction.Always, false));
			_psoTonemapOff = new GraphicsPipeline(Device,
				Device.GetShader("fullscreenSimple.vert"),
				Device.GetShader("tonemap.frag", new List<Tuple<string, string>>()
				{
					new Tuple<string, string>("NO_TONEMAP_GAMMA", "1"),
				}),
				null, new RasterizerState(), new DepthState(DepthFunction.Always, false));
			
			_psoCompose = new ComputePipeline(Device, Device.GetShader("compose.comp"));
			_psoLightShadow = new ComputePipeline(Device, Device.GetShader("shadow.comp"));
		}

		public void OnUpdateWorld(Game.World world)
		{
			_moduleWorld.FromWorld(world);
		}

		public void OnResize(int width, int height)
		{
			//Console.WriteLine("Resize " + width + " " + height);

			width = width > 16 ? width : 16;
			height = height > 16 ? height : 16;

			Context.DisplayWidth = width;
			Context.RenderWidth = width;
			Context.DisplayHeight = height;
			Context.RenderHeight = height;

			_resize = true;
		}

		long _frame = 0;

		public void DrawFrame(double deltaTime, double ellapsed, double realDeltaT, Camera camera)
		{
			_frame++;
			var currentBlueNoise = _texBlueNoiseArray[(_frame % _texBlueNoiseArray.Length)];

			Context.DeltaT = deltaTime;
			Context.Ellapsed = ellapsed;
			Context.RealDeltaT = realDeltaT;

			if (_cameraHistory == null)
				_cameraHistory = camera;

			if(_resize)
			{
				var preset = _qualityPresets[_qualityPreset];
				Device.Invalidate();
				MakeFbos(Context.RenderWidth, Context.RenderHeight);
				_moduleSmaa.Resize(Context.DisplayWidth, Context.DisplayHeight);
				_moduleGbuffer.Resize(Context.DisplayWidth, Context.DisplayHeight);
				_moduleDiffuse.Resize(Context.DisplayWidth, Context.DisplayHeight, preset[0]);
				_moduleSpecular.Resize(Context.DisplayWidth, Context.DisplayHeight, preset[1], preset[2]);
				_resize = false;
				Device.Invalidate();
			}

			_moduleGbuffer.Swap();

			Device.BindPipeline(_psoClear);
			Device.BindFrameBuffer(FrameBuffer.BackBuffer, FramebufferTarget.Framebuffer);
			GL.Viewport(0, 0, Context.DisplayWidth, Context.DisplayHeight);
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit | ClearBufferMask.StencilBufferBit);

			var queryFrame = Device.CreateStartQuery("Frame");

			Context.SunLight = _moduleAtmosphere.GetColoredSun(Context.SunDir);

			_moduleAtmosphere.RenderAtmosphereEnvmap(_texSky, Context.SunDir);

			_moduleWorld.UpdateLightmap(_texSky, currentBlueNoise);

			if(true)
			{
				RenderComplex(camera, currentBlueNoise);
			}
			else
			{
				_moduleGbuffer.Draw(_moduleWorld, _texSky, camera);

				Device.BindFrameBuffer(FrameBuffer.BackBuffer, FramebufferTarget.Framebuffer);

				Device.BindPipeline(_psoTonemapOff);
				Device.BindTexture(_moduleGbuffer.TexGbufferNormal, 0);
				GL.DrawArrays(PrimitiveType.Triangles, 0, 3);
			}

			Device.BindTexture(TextureTarget.Texture2D, 0, 0);

			queryFrame.EndQuery();

			_cameraHistory = camera;
		}

		void RenderComplex(Camera camera, Texture2D blueNouse)
		{
			using (Device.DebugMessageManager.PushGroupMarker("Main scene render"))
			{
				_moduleGbuffer.Draw(_moduleWorld, _texSky, camera);

				//_moduleGbuffer.DrawSelection(camera, new Vector3i(32), new Vector3i(63), 0);

				var queryShadow = Device.CreateStartQuery("Shadow");

				//
				// Shadow ray
				//
				Device.BindPipeline(_psoLightShadow);
				Context.SetSceneUniforms(Device.ShaderCompute);
				camera.SetUniforms(Device.ShaderCompute);
				_moduleWorld.SetWorldSize(Device.ShaderCompute);
				Device.ShaderCompute.SetUniformF("noiseOffset", (float)(Context.Ellapsed - Math.Floor(Context.Ellapsed)));
				Device.ShaderCompute.SetUniformF("sizercp", new Vector2(1f / _texShadowRaw.Width, 1f / _texShadowRaw.Height));
				Device.BindImage2D(0, _texShadowRaw, TextureAccess.WriteOnly);
				Device.BindTexture(_moduleWorld.TexWorld, 0);
				Device.BindTexture(_moduleWorld.TexWorldMaterial, 3);
				Device.BindTexture(_moduleGbuffer.TexGbufferNormal, 1);
				Device.BindTexture(_moduleGbuffer.TexGbufferPosition, 2);
				Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 7, _moduleWorld.BufferPallete);
				Device.DispatchComputeThreads(_texShadowRaw.Width, _texShadowRaw.Height);

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);

				queryShadow.EndQuery();

				//
				// Diffuse GI
				//
				_moduleDiffuse.CalcGITrace(_moduleWorld, _moduleGbuffer, _moduleUpscaler, _texSky, blueNouse, camera, _cameraHistory);

				//
				// Specular reflections
				//

				_moduleSpecular.CalcSpecular(_moduleWorld, _moduleGbuffer, _moduleUpscaler, _texSky, blueNouse, camera, _cameraHistory);


				//
				// Compose
				//
				var queryCompose = Device.CreateStartQuery("Compose");

				var tmpexp = _bufferExposureA;
				_bufferExposureA = _bufferExposureB;
				_bufferExposureB = tmpexp;
				GlobCore.Utils.CopyBuffer(_bufferHistogramZeroes, _bufferHistogram,
					0, 0, HistogramStops * 4);

				Device.BindPipeline(_psoCompose);
				Context.SetSceneUniforms(Device.ShaderCompute);
				camera.SetUniforms(Device.ShaderCompute);
				_moduleWorld.SetWorldSize(Device.ShaderCompute);
				Device.ShaderCompute.SetUniformF("sizercp", _texSceneComposed.SizeReciprocal);
				Device.ShaderCompute.SetUniformI("histogramBuckets", HistogramStops);
				Device.ShaderCompute.SetUniformF("histogramStart", HistogramStart);
				Device.ShaderCompute.SetUniformF("histogramRange", HistogramRange);
				Device.BindImage2D(0, _texSceneComposed, TextureAccess.WriteOnly);
				Device.BindTexture(_moduleGbuffer.TexGbufferDiffuse, 0);
				Device.BindTexture(_moduleDiffuse.TexDiffuse, 1);
				Device.BindTexture(_moduleGbuffer.TexGbufferGlow, 2);
				Device.BindTexture(_moduleGbuffer.TexGbufferNormal, 3);
				Device.BindTexture(_texShadowRaw, 4);
				Device.BindTexture(_moduleGbuffer.TexGbufferDepth, 5);
				Device.BindTexture(_moduleSpecular.TexSpecular, 6);
				Device.BindTexture(_moduleGbuffer.TexGbufferPosition, 7);
				Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 0, _bufferHistogram);
				Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 1, _bufferExposureA);
				Device.DispatchComputeThreads(_texSceneComposed.Width, _texSceneComposed.Height);

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit
					| MemoryBarrierFlags.TextureFetchBarrierBit | MemoryBarrierFlags.ShaderStorageBarrierBit);

				Device.BindPipeline(_psoExposure);

				Device.ShaderCompute.SetUniformI("histogramBuckets", HistogramStops);
				Device.ShaderCompute.SetUniformF("histogramStart", HistogramStart);
				Device.ShaderCompute.SetUniformF("histogramRange", HistogramRange);
				Device.ShaderCompute.SetUniformF("pixelsRcp", 1f / (_texSceneComposed.Width * _texSceneComposed.Height));
				Device.ShaderCompute.SetUniformF("deltaTime", (float)Context.DeltaT);

				Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 0, _bufferHistogram);
				Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 1, _bufferExposureA);
				Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 2, _bufferExposureB);

				Device.DispatchComputeGroups(1);

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderStorageBarrierBit);

				queryCompose.EndQuery();
			}

			GL.Viewport(0, 0, Context.DisplayWidth, Context.DisplayHeight);

			// SMAA toggle
			if (true)
			{
				Device.BindFrameBuffer(_fboTonemap, FramebufferTarget.Framebuffer);

				Device.BindPipeline(_psoTonemap);
				Device.BindTexture(_texSceneComposed, 0);
				GL.DrawArrays(PrimitiveType.Triangles, 0, 3);

				_moduleSmaa.Render(_texSceneTonemapped, _moduleGbuffer.TexGbufferDepth, FrameBuffer.BackBuffer);
			}
			else
			{
				Device.BindFrameBuffer(FrameBuffer.BackBuffer, FramebufferTarget.Framebuffer);

				Device.BindPipeline(_psoTonemap);
				Device.BindTexture(_texSceneComposed, 0);
				GL.DrawArrays(PrimitiveType.Triangles, 0, 3);
			}
		}

		void MakeFbos(int w, int h)
		{
			w = w > 16 ? w : 16;
			h = h > 16 ? h : 16;

			int sceneW = Math.Max(1, w);
			int sceneH = Math.Max(1, h);

			_fboTonemap?.Dispose();
			_texSceneComposed?.Dispose();
			_texShadowRaw?.Dispose();
			_texSceneTonemapped?.Dispose();
			
			_texShadowRaw = new Texture2D(Device, "Shadow Raw", SizedInternalFormatGlob.R8, sceneW, sceneH, 1);
			_texSceneTonemapped = new Texture2D(Device, "Scene Tonemapped", SizedInternalFormatGlob.RGBA16F, sceneW, sceneH, 1);
			_texSceneComposed = new Texture2D(Device, "Scene Composed", SizedInternalFormatGlob.RGBA16F, sceneW, sceneH, 1);

			_fboTonemap = new FrameBuffer();
			Device.BindFrameBuffer(_fboTonemap, FramebufferTarget.Framebuffer);
			_fboTonemap.Attach(FramebufferAttachment.ColorAttachment0, _texSceneTonemapped);

			Device.BindFrameBuffer(FrameBuffer.BackBuffer, FramebufferTarget.Framebuffer);
			Device.BindTexture(TextureTarget.Texture2D, 0, 0);
		}

		Texture2D[] LoadBlueNoise()
		{
			List<Texture2D> textures = new List<Texture2D>();
			for(int i = 0; i < 16; i++)
			{
				using (var stream = System.IO.File.OpenRead("EngineTextures/LDR_RGBA_" + i + ".png"))
				{
					if (stream == null)
					{
						continue;
					}
					
					using (var bitmap = (Bitmap)Image.FromStream(stream))
					{
						var data = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), System.Drawing.Imaging.ImageLockMode.ReadOnly,
							System.Drawing.Imaging.PixelFormat.Format32bppRgb);
						var tex = new Texture2D(this.Context.Device, "BlueNoise", SizedInternalFormatGlob.RGBA8, bitmap.Width,
							bitmap.Height, 1);
						tex.TexSubImage2D(Device, 0, 0, 0, tex.Width, tex.Height, PixelFormat.Rgba, PixelType.UnsignedByte,
							data.Scan0);
						bitmap.UnlockBits(data);
						textures.Add(tex);
					}
				}
			}
			return textures.ToArray();
		}

		public List<Texture2D> GetDebugTextures()
		{
			return new List<Texture2D>()
			{
				_texSceneComposed,
				_texSceneTonemapped,
				_texShadowRaw,
				_moduleGbuffer.TexGbufferDepth,
				_moduleGbuffer.TexGbufferNormal,
				_moduleDiffuse.TexDiffuse,
				_moduleSpecular.TexSpecular,
			};
		}
	}
}
