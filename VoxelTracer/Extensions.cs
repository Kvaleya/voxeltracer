﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Mathematics;

namespace VoxelTracer
{
	static class Extensions
	{
		public static Vector3i ToIvec(this Vector3 v)
		{
			return new Vector3i((int)v.X, (int)v.Y, (int)v.Z);
		}

		public static Vector3i ToIvecFloor(this Vector3 v)
		{
			return new Vector3i((int)Math.Floor(v.X), (int)Math.Floor(v.Y), (int)Math.Floor(v.Z));
		}

		public static string ToMs(this double v)
		{
			return (v * 1000).ToString("00.00", System.Globalization.CultureInfo.InvariantCulture);
		}

		public static string ToMs(this float v)
		{
			return ((double)v).ToMs();
		}
	}
}
