﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Mathematics;
using OpenTK.Windowing.GraphicsLibraryFramework;

namespace VoxelTracer
{
	class CameraControls
	{
		public Game.Player Controller;
		public Vector3 Position = new Vector3(32, 50, -8);
		public float HorizontalFov = 100f; // In degrees
		public double Pitch = 0, Yaw = 0; // In radians

		// Positive yaw = looking right
		public double YawDegrees { get { return Yaw * 180 / Math.PI; } set { Yaw = value / 180 * Math.PI; } }
		// Positive pitch = looking up
		public double PitchDegrees { get { return Pitch * 180 / Math.PI; } set { Pitch = value / 180 * Math.PI; } }

		public Vector2 MouseSensitivity = new Vector2(0.1f);
		public float MoveSpeed = 16.0f;

		public Keys KeyForward = Keys.W, KeyBack = Keys.S, KeyLeft = Keys.A,
			KeyRight = Keys.D, KeyUp = Keys.E, KeyDown = Keys.Q,
			KeySprint = Keys.LeftShift, KeyWalk = Keys.LeftAlt;

		public Vector3 Forward
		{
			get
			{
				return new Vector3(
					(float)(Math.Sin(-Yaw) * Math.Cos(Pitch)),
					(float)(Math.Sin(Pitch)),
					(float)(Math.Cos(Yaw) * Math.Cos(Pitch))
				);
			}
		}

		public Vector3 Right { get { return Vector3.Cross(Forward, Vector3.UnitY).Normalized(); } }

		public Camera GetCamera(int width, int height)
		{
			return new Camera(Position, HorizontalFov, width / (float)height, Forward);
		}

		public void UpdateControlsFPS(InputState input, double deltaTime)
		{
			// Camera look
			YawDegrees += input.Mouse.Delta.X * MouseSensitivity.X;
			PitchDegrees -= input.Mouse.Delta.Y * MouseSensitivity.Y;

			while (Yaw < 0)
				Yaw += Math.PI * 2;
			while (Yaw > Math.PI * 2)
				Yaw -= Math.PI * 2;

			double pitchLimit = Math.PI * 0.49;

			if (Pitch > pitchLimit)
				Pitch = pitchLimit;
			if (Pitch < -pitchLimit)
				Pitch = -pitchLimit;

			// Camera movement
			Vector3 move = Vector3.Zero;

			if (input.Keyboard.IsKeyDown(KeyForward))
				move.Z += 1;
			if (input.Keyboard.IsKeyDown(KeyBack))
				move.Z -= 1;
			if (input.Keyboard.IsKeyDown(KeyRight))
				move.X += 1;
			if (input.Keyboard.IsKeyDown(KeyLeft))
				move.X -= 1;
			if (input.Keyboard.IsKeyDown(KeyUp))
				move.Y += 1;
			if (input.Keyboard.IsKeyDown(KeyDown))
				move.Y -= 1;

			if (input.Keyboard.IsKeyDown(KeySprint))
				move *= 8;
			if (input.Keyboard.IsKeyDown(KeyWalk))
				move /= 8;

			if (input.Keyboard.IsKeyDown(Keys.C) && !input.Keyboard.WasKeyDown(Keys.C))
				Controller.ControlState = Controller.ControlState == Game.PlayerControlState.Walk ? Game.PlayerControlState.FlightNoClip : Game.PlayerControlState.Walk;

			move *= (float)deltaTime * MoveSpeed;

			Vector3 delta = Vector3.Zero;

			delta += Forward * move.Z;
			delta += Right * move.X;
			delta += Vector3.UnitY * move.Y;

			//Controller.Update(delta, (float)deltaTime);
			//this.Position = Controller.Position;
		}
	}
}
