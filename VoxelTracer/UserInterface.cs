﻿using System;
using System.Collections.Generic;
using System.Text;
using ImGuiNET;
using GlobCore;

namespace VoxelTracer
{
	class UserInterface
	{
		const int MaxTimerDatapoints = 32;
		static Dictionary<string, Queue<double>> _timers = new Dictionary<string, Queue<double>>();

		static void ProcessAndDrawQueries(IReadOnlyList<GlobCore.TimerQuery> queries)
		{
			foreach(var q in queries)
			{
				if (!_timers.ContainsKey(q.Name))
					_timers.Add(q.Name, new Queue<double>());
				_timers[q.Name].Enqueue(q.Ellapsed);
				if (_timers[q.Name].Count > MaxTimerDatapoints)
					_timers[q.Name].Dequeue();
			}

			foreach(var pair in _timers)
			{
				double avg = 0;
				double min = double.PositiveInfinity;
				double max = double.NegativeInfinity;
				foreach(var d in pair.Value)
				{
					min = Math.Min(min, d);
					max = Math.Max(max, d);
					avg += d;
				}
				avg /= pair.Value.Count;

				ImGui.Text(avg.ToMs() + " " + pair.Key + " (min: "
					+ min.ToMs() + " max: "
					+ max.ToMs() + " real: "
					+ pair.Value.Peek().ToMs() + ")"
				);
			}
		}

		static string BytesToSizeNice(long bytes)
		{
			// https://stackoverflow.com/questions/281640/how-do-i-get-a-human-readable-file-size-in-bytes-abbreviation-using-net
			// https://stackoverflow.com/a/281679
			string[] sizes = { "B", "KiB", "MiB", "GiB", "TiB" };
			double len = bytes;
			int order = 0;
			while (len >= 1024 && order < sizes.Length - 1)
			{
				order++;
				len = len / 1024;
			}

			// Adjust the format string to your preferences. For example "{0:0.#}{1}" would
			// show a single decimal place, and no space.
			return String.Format("{0:0.##} {1}", len, sizes[order]);
		}

		static void DebugTexView(ImGuiRenderer imgui, List<Texture2D> textures)
		{
			for(int i = 0; i < textures.Count; i++)
			{
				var tex = textures[i];
				if (tex == null)
					continue;
				ImGui.Text(tex.ToString());
				ImGui.Image(imgui.GetTexture(tex, false), new System.Numerics.Vector2(320, 180), 
					new System.Numerics.Vector2(0, 1), new System.Numerics.Vector2(1, 0)
				);
			}
		}

		public static void Draw(GlobCore.Device device, ImGuiRenderer imguiRenderer, WorldRenderer worldRenderer, double[] frameTimes)
		{
			ImGui.SetNextWindowSize(new System.Numerics.Vector2(550, 300), ImGuiCond.FirstUseEver);
			ImGui.Begin("Debug");

			float[] times = new float[frameTimes.Length];
			for (int i = 0; i < frameTimes.Length; i++)
				times[i] = (float)frameTimes[i];

			//ImGui.PlotLines("Frametimes", ref times[0], times.Length, 0, "", 0, 0.1f, new System.Numerics.Vector2(400, 200));

			ImGui.Text("GameDT: " + worldRenderer.Context.DeltaT.ToMs());
			ImGui.Text("RealDT: " + worldRenderer.Context.RealDeltaT.ToMs());
			ImGui.Text("WinwDT: " + MainLoop.Instance.Window.LastTimed.ToMs());
			ImGui.Text("SwapDT: " + MainLoop.Instance.Window.LastTimedWithSwap.ToMs());
			ImGui.Text($"GC: Gen 0: {GC.CollectionCount(0)} Gen 1: {GC.CollectionCount(1)} Gen 2: {GC.CollectionCount(2)}");
			ImGui.Text("Tex: " + BytesToSizeNice(device.TextureBytesApprox));

			ProcessAndDrawQueries(device.TimerQueries);

			DebugTexView(imguiRenderer, worldRenderer.GetDebugTextures());

			ImGui.End();
		}
	}
}
