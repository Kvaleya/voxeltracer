﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Mathematics;
using OpenTK.Graphics.OpenGL;
using GlobCore;

namespace VoxelTracer.Rendering
{
	class ModuleWorld : RenderModule
	{
		public Texture3D TexWorld { get; private set; }
		Texture3D _texWorldSwap;
		public Texture3D TexWorldMaterial { get; private set; }

		public int BufferPallete { get; private set; }

		public Texture3D TexWorldLightmap { get; private set; }
		Texture3D _texWorldLightmapHistory;

		int _bufferOnes;
		int _bufferLightmapDispatch;
		int _bufferLightmapFaces = 0;

		int _sizePowZ = 0;

		ComputePipeline _psoWorldAccel;

		ComputePipeline _psoLightmapDetectFaces;
		ComputePipeline _psoLightmapTrace;

		public ModuleWorld(RenderContext context)
			: base(context)
		{
			_psoWorldAccel = new ComputePipeline(Device, Device.GetShader("worldGenAccel.comp"));

			uint[] ones = new uint[8];

			for (int i = 0; i < ones.Length; i++)
			{
				ones[i] = 1;
			}

			_bufferOnes = GlobCore.Utils.CreateBuffer(BufferTarget.ShaderStorageBuffer, new IntPtr(16), ones, BufferStorageFlags.None, "Lightmap dispatch ones buffer");
			_bufferLightmapDispatch = GlobCore.Utils.CreateBuffer(BufferTarget.ShaderStorageBuffer, new IntPtr(16), ones, BufferStorageFlags.None, "Lightmap dispatch buffer");
		}

		public void FromWorld(Game.World world)
		{
			InitWorld(world.WorldSize, world.WorldSizePow, world.WorldCollisions, world.WorldMaterials, world.PalleteBuffer);
		}

		void InitWorld(int size, int sizepow, byte[] world, byte[] material, uint[] pallete)
		{
			_sizePowZ = sizepow;
			TexWorld?.Dispose();
			_texWorldSwap?.Dispose();
			TexWorldMaterial?.Dispose();
			TexWorldLightmap?.Dispose();
			_texWorldLightmapHistory?.Dispose();

			if (_bufferLightmapFaces > 0)
				GL.DeleteBuffer(_bufferLightmapFaces);

			BufferPallete = GlobCore.Utils.CreateBuffer(BufferTarget.ShaderStorageBuffer, new IntPtr(pallete.Length * 4), pallete, BufferStorageFlags.None, "Color pallete");

			_texWorldSwap = new Texture3D(Device, "World Collisions B", SizedInternalFormatGlob.R8UI, size, size, size, 1);
			TexWorld = new Texture3D(Device, "World Collisions A", SizedInternalFormatGlob.R8UI, size, size, size, 1);
			TexWorld.TexSubImage3D(Device, 0, 0, 0, 0, size, size, size, PixelFormat.RedInteger, PixelType.UnsignedByte, world);

			ComputeWorldAccel(size);

			TexWorldMaterial = new Texture3D(Device, "World Material", SizedInternalFormatGlob.R8UI, size, size, size, 1);
			TexWorldMaterial.TexSubImage3D(Device, 0, 0, 0, 0, size, size, size, PixelFormat.RedInteger, PixelType.UnsignedByte, material);

			TexWorldLightmap = new Texture3D(Device, "World Lightmap A", SizedInternalFormatGlob.RGBA16F, size, size, size * 6, 1);
			_texWorldLightmapHistory = new Texture3D(Device, "World Lightmap B", SizedInternalFormatGlob.RGBA16F, size, size, size * 6, 1);

			int maxlightfaces = size * size * size * 3;

			GlobCore.Utils.SetTextureParameters(Device, _texWorldSwap, TextureWrapMode.ClampToEdge,
				TextureMagFilter.Nearest, TextureMinFilter.Nearest);
			GlobCore.Utils.SetTextureParameters(Device, TexWorld, TextureWrapMode.ClampToEdge,
				TextureMagFilter.Nearest, TextureMinFilter.Nearest);
			GlobCore.Utils.SetTextureParameters(Device, TexWorldMaterial, TextureWrapMode.ClampToEdge,
				TextureMagFilter.Nearest, TextureMinFilter.Nearest);

			_bufferLightmapFaces = GlobCore.Utils.CreateBuffer(BufferTarget.ShaderStorageBuffer, new IntPtr(maxlightfaces * 4), IntPtr.Zero, BufferStorageFlags.None, "Lightmap faces buffer");

			_psoLightmapDetectFaces = new ComputePipeline(Device, Device.GetShader("lightmapDetectFaces.comp"));
			_psoLightmapTrace = new ComputePipeline(Device, Device.GetShader("lightmapTrace.comp"));
		}

		void ComputeWorldAccel(int size)
		{
			var query = Device.CreateStartQuery("World accel struct gen");
			ComputeAccelerationIncremental(true);
			/*
			for (int r = 2; r < size; r++)
			{
				ComputeAccelerationIncremental(r == 2);
			}
			*/
			query.EndQuery();
		}

		public void ComputeAccelerationIncremental(bool clear = false)
		{
			var tmp = _texWorldSwap;
			_texWorldSwap = TexWorld;
			TexWorld = tmp;

			Device.BindPipeline(_psoWorldAccel);
			Device.ShaderCompute.SetUniformI("worldSize", TexWorld.Width, TexWorld.Height, TexWorld.Depth);
			Device.ShaderCompute.SetUniformI("clearAccel", clear ? 1 : 0);
			
			Device.BindImage3D(0, TexWorld, TextureAccess.WriteOnly);
			Device.BindImage3D(1, _texWorldSwap, TextureAccess.ReadOnly);

			Device.ShaderCompute.SetUniformI("groupOffset", 0, 0, 0);
			Device.DispatchComputeThreads(TexWorld.Width, TexWorld.Height, TexWorld.Depth);

			GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);
		}

		public void UpdateLightmap(TextureCube texSky, Texture2D texBlueNoise)
		{
			ComputeAccelerationIncremental(false);

			var lightmaptmp = _texWorldLightmapHistory;
			_texWorldLightmapHistory = TexWorldLightmap;
			TexWorldLightmap = lightmaptmp;

			var queryLightmap = Device.CreateStartQuery("Lightmap update");

			using (Device.DebugMessageManager.PushGroupMarker("Lightmap update"))
			{
				// Clear dispatch buffer
				GlobCore.Utils.CopyBuffer(_bufferOnes, _bufferLightmapDispatch, 0, 0, 16);

				// Detect unobscured faces
				Device.BindPipeline(_psoLightmapDetectFaces);
				SetWorldSize(Device.ShaderCompute);
				Device.ShaderCompute.SetUniformI("wsizezpowz", _sizePowZ);
				Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 0, _bufferLightmapDispatch);
				Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 1, _bufferLightmapFaces);
				Device.BindTexture(TexWorld, 0);
				Device.DispatchComputeThreads(TexWorldLightmap.Width, TexWorldLightmap.Height, TexWorldLightmap.Depth);

				GL.BindBuffer(BufferTarget.DispatchIndirectBuffer, _bufferLightmapDispatch);
				GL.MemoryBarrier(MemoryBarrierFlags.ShaderStorageBarrierBit | MemoryBarrierFlags.CommandBarrierBit);

				// Trace rays for lightmap faces

				Device.BindPipeline(_psoLightmapTrace);
				RenderContext.SetSceneUniforms(Device.ShaderCompute);
				SetWorldSize(Device.ShaderCompute);
				Device.ShaderCompute.SetUniformF("ellapsed", (float)RenderContext.Ellapsed);
				Device.ShaderCompute.SetUniformF("noiseOffset", (float)(RenderContext.NoiseEllapsed));
				Device.BindImage3D(0, TexWorldLightmap, TextureAccess.WriteOnly);
				Device.BindTexture(TexWorld, 0);
				Device.BindTexture(TexWorldMaterial, 1);
				Device.BindTexture(_texWorldLightmapHistory, 2);
				Device.BindTexture(texBlueNoise, 3);
				Device.BindTexture(texSky, 4);
				Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 7, BufferPallete);

				if (Device.ShaderCompute.WorkGroupSizeX > 0)
					GL.DispatchComputeIndirect(IntPtr.Zero);

				GL.BindBuffer(BufferTarget.DispatchIndirectBuffer, 0);
				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);
			}

			queryLightmap.EndQuery();
		}

		public void SetWorldSize(Shader s)
		{
			s.SetUniformI("worldSize", TexWorld.Width, TexWorld.Height, TexWorld.Depth);
		}
	}
}
