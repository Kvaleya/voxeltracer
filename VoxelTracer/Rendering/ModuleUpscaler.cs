﻿using System;
using System.Collections.Generic;
using System.Text;
using GlobCore;
using OpenTK.Graphics.OpenGL;

namespace VoxelTracer.Rendering
{
	class ModuleUpscaler : RenderModule
	{
		ComputePipeline _psoUpscale;

		public ModuleUpscaler(RenderContext context)
			: base(context)
		{
			_psoUpscale = new ComputePipeline(Device, Device.GetShader("upscalegi.comp"));
		}

		public void Upscale(ModuleGbuffer gbuffer, Camera camera, Texture2D input, int resshift, Texture2D output)
		{
			Device.BindPipeline(_psoUpscale);
			camera.SetUniforms(Device.ShaderCompute);
			Device.ShaderCompute.SetUniformI("resshift", resshift);
			Device.ShaderCompute.SetUniformI("inputSize", input.Width, input.Height);
			Device.ShaderCompute.SetUniformF("sizercp", output.SizeReciprocal);
			Device.BindImage2D(0, output, TextureAccess.WriteOnly);
			Device.BindTexture(input, 0);
			Device.BindTexture(gbuffer.TexGbufferNormal, 1);
			Device.BindTexture(gbuffer.TexGbufferDepth, 2);
			Device.DispatchComputeThreads(output);
			GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);
		}
	}
}
