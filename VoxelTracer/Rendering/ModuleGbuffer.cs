﻿using System;
using System.Collections.Generic;
using System.Text;
using GlobCore;
using OpenTK.Mathematics;
using OpenTK.Graphics.OpenGL;

namespace VoxelTracer.Rendering
{
	class ModuleGbuffer : RenderModule
	{
		public FrameBuffer FboGbuffer { get; private set; }
		FrameBuffer _fboGbufferHistory;
		public Texture2D TexGbufferDiffuse { get; private set; }
		public Texture2D TexGbufferDepth { get; private set; }
		public Texture2D TexGbufferDepthHistory { get; private set; }
		public Texture2D TexGbufferPosition { get; private set; }
		public Texture2D TexGbufferNormal { get; private set; }
		public Texture2D TexGbufferNormalHistory { get; private set; }
		public Texture2D TexGbufferGlow { get; private set; }

		GraphicsPipeline _psoGbuffer;
		GraphicsPipeline _psoClear;

		ComputePipeline _psoHighlight;

		bool _clear = true;

		public ModuleGbuffer(RenderContext context)
			: base(context)
		{
			_psoClear = new GraphicsPipeline(Device, null, null,
				null, new RasterizerState(), new DepthState(DepthFunction.Always, true));
			_psoGbuffer = new GraphicsPipeline(Device,
				Device.GetShader("fullscreenSimple.vert"),
				Device.GetShader("gbuffer.frag"),
				null, new RasterizerState(), new DepthState(DepthFunction.Always, false));
			_psoHighlight = new ComputePipeline(Device, Device.GetShader("highlight.comp"));
		}

		public void Swap()
		{
			var fbotmp = _fboGbufferHistory;
			_fboGbufferHistory = FboGbuffer;
			FboGbuffer = fbotmp;

			var depthtmp = TexGbufferDepthHistory;
			TexGbufferDepthHistory = TexGbufferDepth;
			TexGbufferDepth = depthtmp;

			var normaltmp = TexGbufferNormalHistory;
			TexGbufferNormalHistory = TexGbufferNormal;
			TexGbufferNormal = normaltmp;
		}

		public void Draw(ModuleWorld world, TextureCube texSky, Camera camera)
		{
			Device.BindFrameBuffer(FboGbuffer, FramebufferTarget.Framebuffer);

			GL.DrawBuffers(5, new[]
			{
					DrawBuffersEnum.ColorAttachment0,
					DrawBuffersEnum.ColorAttachment1,
					DrawBuffersEnum.ColorAttachment2,
					DrawBuffersEnum.ColorAttachment3,
					DrawBuffersEnum.ColorAttachment4,
				});

			GL.Viewport(0, 0, TexGbufferDiffuse.Width, TexGbufferDiffuse.Height);

			if (_clear)
			{
				Device.BindPipeline(_psoClear);
				GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit | ClearBufferMask.StencilBufferBit);
				_clear = false;
			}

			var queryGbuffer = Device.CreateStartQuery("Gbuffer");

			//
			// Fill gbuffer
			//
			Device.BindPipeline(_psoGbuffer);
			camera.SetUniforms(Device.ShaderFragment);
			world.SetWorldSize(Device.ShaderFragment);
			Device.ShaderFragment.SetUniformF("ellapsed", (float)RenderContext.Ellapsed);
			Device.BindTexture(world.TexWorld, 0);
			Device.BindTexture(world.TexWorldMaterial, 1);
			Device.BindTexture(texSky, 2);
			Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 7, world.BufferPallete);

			GL.DrawArrays(PrimitiveType.Triangles, 0, 3);

			GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);

			queryGbuffer.EndQuery();
		}

		public void DrawSelection(Camera camera, Vector3i min, Vector3i max, int face)
		{
			Device.BindPipeline(_psoHighlight);
			
			Device.ShaderCompute.SetUniformI("selectionMin", min);
			Device.ShaderCompute.SetUniformI("selectionMax", max);
			Device.ShaderCompute.SetUniformI("selectionFace", face);
			Device.ShaderCompute.SetUniformF("sizercp", TexGbufferGlow.SizeReciprocal);
			camera.SetUniforms(Device.ShaderCompute);

			Device.BindTexture(TexGbufferPosition, 0);
			Device.BindImage2D(0, TexGbufferGlow, TextureAccess.WriteOnly);
			
			Device.DispatchComputeThreads(TexGbufferGlow);

			GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);
		}

		public void Resize(int w, int h)
		{
			_clear = true;

			int sceneW = Math.Max(1, w);
			int sceneH = Math.Max(1, h);

			FboGbuffer?.Dispose();
			_fboGbufferHistory?.Dispose();
			
			TexGbufferDiffuse?.Dispose();
			TexGbufferPosition?.Dispose();
			TexGbufferDepth?.Dispose();
			TexGbufferDepthHistory?.Dispose();
			TexGbufferNormal?.Dispose();
			TexGbufferNormalHistory?.Dispose();
			TexGbufferGlow?.Dispose();

			TexGbufferDiffuse = new Texture2D(Device, "Gbuffer Diffuse", SizedInternalFormatGlob.RGBA8, sceneW, sceneH, 1);
			TexGbufferDepth = new Texture2D(Device, "Gbuffer Depth A", SizedInternalFormatGlob.R16F, sceneW, sceneH, 1);
			TexGbufferDepthHistory = new Texture2D(Device, "Gbuffer Depth B", SizedInternalFormatGlob.R16F, sceneW, sceneH, 1);
			TexGbufferPosition = new Texture2D(Device, "Gbuffer Voxel Position", SizedInternalFormatGlob.RGBA32UI, sceneW, sceneH, 1);
			TexGbufferNormal = new Texture2D(Device, "Gbuffer Normal A", SizedInternalFormatGlob.RGB10_A2, sceneW, sceneH, 1);
			TexGbufferNormalHistory = new Texture2D(Device, "Gbuffer Normal B", SizedInternalFormatGlob.RGB10_A2, sceneW, sceneH, 1);
			TexGbufferGlow = new Texture2D(Device, "Gbuffer Glow", SizedInternalFormatGlob.RGBA16F, sceneW, sceneH, 1);

			FboGbuffer = new FrameBuffer();
			Device.BindFrameBuffer(FboGbuffer, FramebufferTarget.Framebuffer);
			FboGbuffer.Attach(FramebufferAttachment.ColorAttachment0, TexGbufferDiffuse);
			FboGbuffer.Attach(FramebufferAttachment.ColorAttachment1, TexGbufferDepth);
			FboGbuffer.Attach(FramebufferAttachment.ColorAttachment2, TexGbufferNormal);
			FboGbuffer.Attach(FramebufferAttachment.ColorAttachment3, TexGbufferGlow);
			FboGbuffer.Attach(FramebufferAttachment.ColorAttachment4, TexGbufferPosition);

			_fboGbufferHistory = new FrameBuffer();
			Device.BindFrameBuffer(_fboGbufferHistory, FramebufferTarget.Framebuffer);
			_fboGbufferHistory.Attach(FramebufferAttachment.ColorAttachment0, TexGbufferDiffuse);
			_fboGbufferHistory.Attach(FramebufferAttachment.ColorAttachment1, TexGbufferDepthHistory);
			_fboGbufferHistory.Attach(FramebufferAttachment.ColorAttachment2, TexGbufferNormalHistory);
			_fboGbufferHistory.Attach(FramebufferAttachment.ColorAttachment3, TexGbufferGlow);
			_fboGbufferHistory.Attach(FramebufferAttachment.ColorAttachment4, TexGbufferPosition);

			Device.BindFrameBuffer(FrameBuffer.BackBuffer, FramebufferTarget.Framebuffer);
			Device.BindTexture(TextureTarget.Texture2D, 0, 0);
		}
	}
}
