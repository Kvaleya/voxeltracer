﻿using System;
using System.Collections.Generic;
using System.Text;
using GlobCore;
using OpenTK.Mathematics;
using OpenTK.Graphics.OpenGL;

namespace VoxelTracer.Rendering
{
	class ModuleSpecular : RenderModule
	{
		public int ResShiftTrace { get; private set; } = 0;
		public int ResShiftResolve { get; private set; } = 0;

		public Texture2D TexSpecular { get { return ResShiftResolve > 0 ? _texSpecularFinal : _texSpecular; } }

		int _bufferLightTilesA;
		int _bufferLightTilesB;

		int _tilesX, tilesY;

		Texture2D _texRayCacheParams;
		Texture2D _texRayCacheColorDepth;
		Texture2D _texSpecularHistory;
		Texture2D _texSpecularSpacial;
		Texture2D _texSpecular;
		Texture2D _texSpecularFinal;

		const int PreintegratedBRDFSize = 512;
		Texture2D _texPreintegratedBRDF;

		ComputePipeline _psoLightSpecular;
		ComputePipeline _psoResolveTemporal;
		ComputePipeline[] _psoResolveSpacialArray;
		ComputePipeline _psoPreintegrateBRDF;

		public ModuleSpecular(RenderContext context)
			: base(context)
		{
			_psoLightSpecular = new ComputePipeline(Device, Device.GetShader("specular.comp"));
			_psoResolveTemporal = new ComputePipeline(Device, Device.GetShader("specularResolveTemporal.comp"));
			_psoPreintegrateBRDF = new ComputePipeline(Device, Device.GetShader("preintegrateBrdf.comp"));

			_psoResolveSpacialArray = new ComputePipeline[2];
			for (int i = 0; i < _psoResolveSpacialArray.Length; i++)
			{
				_psoResolveSpacialArray[i] = new ComputePipeline(Device, Device.GetShader("specularResolveSpacial.comp", new List<Tuple<string, string>>()
				{
					new Tuple<string, string>("RANGE", (i+1).ToString())
				}));
			}

			_texPreintegratedBRDF = new Texture2D(Device, "Preintegrated BRDF", SizedInternalFormatGlob.RG16, PreintegratedBRDFSize, PreintegratedBRDFSize, 1);
			GlobCore.Utils.SetTextureParameters(Device, _texPreintegratedBRDF, TextureWrapMode.ClampToEdge, TextureMagFilter.Linear, TextureMinFilter.Linear);
			
			//PreintegrateBRDF();
		}

		void PreintegrateBRDF()
		{
			var query = Device.CreateStartQuery("BRDF preintegrate");

			Device.BindPipeline(_psoPreintegrateBRDF);
			Device.ShaderCompute.SetUniformF("sizercp", new Vector2(1f / _texPreintegratedBRDF.Width, 1f / _texPreintegratedBRDF.Height));
			Device.BindImage2D(0, _texPreintegratedBRDF, TextureAccess.WriteOnly);
			Device.DispatchComputeThreads(_texPreintegratedBRDF.Width, _texPreintegratedBRDF.Height);

			GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);

			query.EndQuery();
		}

		public void Resize(int w, int h, int resshift, int resshiftResolve)
		{
			ResShiftTrace = resshift;
			ResShiftResolve = resshiftResolve;

			_texSpecularFinal?.Dispose();
			_texSpecularFinal = new Texture2D(Device, "Light Specular Final", SizedInternalFormatGlob.RGBA16F, w, h, 1);

			int sw = w >> ResShiftTrace;
			int sh = h >> ResShiftTrace;

			sw = sw > 2 ? sw : 2;
			sh = sh > 2 ? sh : 2;

			w = w >> resshiftResolve;
			h = h >> resshiftResolve;

			_texSpecularSpacial?.Dispose();
			_texSpecularSpacial = new Texture2D(Device, "Light Specular A", SizedInternalFormatGlob.RGBA16F, w, h, 1);
			
			_texSpecular?.Dispose();
			_texSpecular = new Texture2D(Device, "Light Specular A", SizedInternalFormatGlob.RGBA16F, w, h, 1);

			_texSpecularHistory?.Dispose();
			_texSpecularHistory = new Texture2D(Device, "Light Specular B", SizedInternalFormatGlob.RGBA16F, w, h, 1);

			_texRayCacheColorDepth?.Dispose();
			_texRayCacheColorDepth = new Texture2D(Device, "Specular Ray Cache Color", SizedInternalFormatGlob.RGBA16F, sw, sh, 1);
			
			_texRayCacheParams?.Dispose();
			_texRayCacheParams = new Texture2D(Device, "Specular Ray Cache Params", SizedInternalFormatGlob.RG32UI, sw, sh, 1);

			if (_bufferLightTilesA > 0)
				GL.DeleteBuffer(_bufferLightTilesA);
			if (_bufferLightTilesB > 0)
				GL.DeleteBuffer(_bufferLightTilesB);
		}

		public void CalcSpecular(ModuleWorld world, ModuleGbuffer gbuffer, ModuleUpscaler upscaler, TextureCube texSky, Texture2D texBlueNoise, Camera camera, Camera cameraHistory)
		{
			var queryAll = Device.CreateStartQuery("Specular Total");

			var tmp = _texSpecularHistory;
			_texSpecularHistory = _texSpecular;
			_texSpecular = tmp;

			using (Device.DebugMessageManager.PushGroupMarker("Specular"))
			{
				var queryTrace = Device.CreateStartQuery("Specular trace");

				//
				// Specular ray trace
				//
				Device.BindPipeline(_psoLightSpecular);
				RenderContext.SetSceneUniforms(Device.ShaderCompute);
				camera.SetUniforms(Device.ShaderCompute);
				world.SetWorldSize(Device.ShaderCompute);
				Device.ShaderCompute.SetUniformF("ellapsed", (float)RenderContext.Ellapsed);
				Device.ShaderCompute.SetUniformF("noiseOffset", (float)RenderContext.NoiseEllapsed);
				Device.ShaderCompute.SetUniformF("sizercp", new Vector2(1f / _texRayCacheColorDepth.Width, 1f / _texRayCacheColorDepth.Height));
				Device.ShaderCompute.SetUniformI("resshift", ResShiftTrace);
				Device.BindImage2D(0, _texRayCacheColorDepth, TextureAccess.WriteOnly);
				Device.BindImage2D(1, _texRayCacheParams, TextureAccess.WriteOnly);
				Device.BindTexture(world.TexWorld, 0);
				Device.BindTexture(gbuffer.TexGbufferNormal, 1);
				Device.BindTexture(gbuffer.TexGbufferPosition, 2);
				Device.BindTexture(world.TexWorldMaterial, 3);
				Device.BindTexture(texBlueNoise, 4);
				Device.BindTexture(world.TexWorldLightmap, 5);
				Device.BindTexture(texSky, 6);
				Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 7, world.BufferPallete);
				Device.DispatchComputeThreads(_texRayCacheColorDepth.Width, _texRayCacheColorDepth.Height);

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);

				queryTrace.EndQuery();
				/*
				var querySpacial = Device.CreateStartQuery("Specular resolve spacial");

				//
				// Resolve spacial
				//
				Device.BindPipeline(_psoResolveSpacialArray[ResShiftResolve == 0 && ResShiftTrace == 0 ? 0 : 1]);
				camera.SetUniforms(Device.ShaderCompute);
				world.SetWorldSize(Device.ShaderCompute);
				Device.ShaderCompute.SetUniformF("sizercp", new Vector2(1f / _texSpecularSpacial.Width, 1f / _texSpecularSpacial.Height));
				Device.ShaderCompute.SetUniformI("texsize", _texSpecularSpacial.Width, _texSpecularSpacial.Height);
				Device.ShaderCompute.SetUniformI("resshift", ResShiftTrace);
				Device.ShaderCompute.SetUniformI("resshiftResolve", ResShiftResolve);
				Device.BindImage2D(0, _texSpecularSpacial, TextureAccess.WriteOnly);
				Device.BindTexture(gbuffer.TexGbufferDepth, 0);
				Device.BindTexture(gbuffer.TexGbufferNormal, 1);
				Device.BindTexture(_texRayCacheColorDepth, 2);
				Device.BindTexture(_texRayCacheParams, 3);
				Device.BindTexture(gbuffer.TexGbufferPosition, 4);
				Device.BindTexture(_texPreintegratedBRDF, 5);
				Device.DispatchComputeThreads(_texSpecularSpacial.Width, _texSpecularSpacial.Height);

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);

				querySpacial.EndQuery();
				*/
				var queryTemporal = Device.CreateStartQuery("Specular resolve temporal");

				//
				// Resolve temporal
				//
				Device.BindPipeline(_psoResolveTemporal);
				camera.SetUniforms(Device.ShaderCompute);
				world.SetWorldSize(Device.ShaderCompute);
				Device.ShaderCompute.SetUniformF("cameraPlaneHistory", cameraHistory.Plane);
				Device.ShaderCompute.SetUniformF("sizercp", new Vector2(1f / _texSpecular.Width, 1f / _texSpecular.Height));
				Device.ShaderCompute.SetUniformF("cameraMatrixHistory", cameraHistory.GetCameraProjectionMatrix());
				Device.ShaderCompute.SetUniformI("texsize", _texSpecular.Width, _texSpecular.Height);
				Device.ShaderCompute.SetUniformI("resshift", ResShiftTrace);
				Device.ShaderCompute.SetUniformI("resshiftResolve", ResShiftResolve);
				cameraHistory.SetUniforms(Device.ShaderCompute, "History");
				Device.BindImage2D(0, _texSpecular, TextureAccess.WriteOnly);
				Device.BindTexture(gbuffer.TexGbufferPosition, 0);
				Device.BindTexture(gbuffer.TexGbufferNormal, 1);
				Device.BindTexture(gbuffer.TexGbufferDepthHistory, 2);
				Device.BindTexture(gbuffer.TexGbufferNormalHistory, 3);
				Device.BindTexture(_texRayCacheColorDepth, 4);
				Device.BindTexture(_texSpecularHistory, 5);
				Device.DispatchComputeThreads(_texSpecular.Width, _texSpecular.Height);

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);

				queryTemporal.EndQuery();

				if (ResShiftResolve > 0)
					upscaler.Upscale(gbuffer, camera, _texSpecular, ResShiftResolve, _texSpecularFinal);
			}

			queryAll.EndQuery();
		}
	}
}
