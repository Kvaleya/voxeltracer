﻿using System;
using System.Collections.Generic;
using System.Text;
using GlobCore;
using OpenTK.Mathematics;
using OpenTK.Graphics.OpenGL;

namespace VoxelTracer.Rendering
{
	class ModuleDiffuse : RenderModule
	{
		public int ResShift { get; private set; } = 0;

		public Texture2D TexDiffuse { get { return ResShift > 0 ? _texDiffuseFinal : _texDiffuseRawA; } }

		Texture2D _texDiffuseHistoryA;
		Texture2D _texDiffuseHistoryB;
		Texture2D _texDiffuseRawA;
		Texture2D _texDiffuseRawB;
		Texture2D _texEdgeStopA;
		Texture2D _texEdgeStopB;

		Texture2D _texDiffuseFinal;

		ComputePipeline _psoLightDiffuse;
		ComputePipeline _psoLightDiffuseAccumulate;
		ComputePipeline _psoLightDiffuseDenoise;

		public ModuleDiffuse(RenderContext context)
			: base(context)
		{
			_psoLightDiffuse = new ComputePipeline(Device, Device.GetShader("diffuse.comp"));
			_psoLightDiffuseAccumulate = new ComputePipeline(Device, Device.GetShader("diffuseAccumulate.comp"));
			_psoLightDiffuseDenoise = new ComputePipeline(Device, Device.GetShader("diffuseDenoise.comp"));
		}

		public void Resize(int w, int h, int resshift)
		{
			ResShift = resshift;

			_texDiffuseHistoryA?.Dispose();
			_texDiffuseHistoryB?.Dispose();
			_texDiffuseRawA?.Dispose();
			_texDiffuseRawB?.Dispose();
			_texEdgeStopA?.Dispose();
			_texEdgeStopB?.Dispose();
			_texDiffuseFinal?.Dispose();

			_texDiffuseFinal = new Texture2D(Device, "Light Diffuse Final", SizedInternalFormatGlob.RGBA16F, w, h, 1);

			w = w >> resshift;
			h = h >> resshift;

			w = w > 4 ? w : 4;
			h = h > 4 ? h : 4;

			_texDiffuseRawA = new Texture2D(Device, "Light Diffuse A", SizedInternalFormatGlob.RGBA16F, w, h, 1);
			_texDiffuseRawB = new Texture2D(Device, "Light Diffuse B", SizedInternalFormatGlob.RGBA16F, w, h, 1);
			_texDiffuseHistoryA = new Texture2D(Device, "Light Diffuse History A", SizedInternalFormatGlob.RGBA16F, w, h, 1);
			_texDiffuseHistoryB = new Texture2D(Device, "Light Diffuse History B", SizedInternalFormatGlob.RGBA16F, w, h, 1);
			_texEdgeStopA = new Texture2D(Device, "Light Diffuse Edgestop A", SizedInternalFormatGlob.RGBA8UI, w, h, 1);
			_texEdgeStopB = new Texture2D(Device, "Light Diffuse Edgestop B", SizedInternalFormatGlob.RGBA8UI, w, h, 1);
		}

		public void CalcGITrace(ModuleWorld world, ModuleGbuffer gbuffer, ModuleUpscaler upscaler, TextureCube texSky, Texture2D texBlueNoise, Camera camera, Camera cameraHistory)
		{
			var textmp = _texDiffuseHistoryA;
			_texDiffuseHistoryA = _texDiffuseHistoryB;
			_texDiffuseHistoryB = textmp;

			var queryAll = Device.CreateStartQuery("Diffuse GI Total");

			using (Device.DebugMessageManager.PushGroupMarker("Diffuse GI"))
			{
				var queryTrace = Device.CreateStartQuery("Diffuse GI trace");

				//
				// Diffuse ray trace
				//
				Device.BindPipeline(_psoLightDiffuse);
				camera.SetUniforms(Device.ShaderCompute);
				world.SetWorldSize(Device.ShaderCompute);
				RenderContext.SetSceneUniforms(Device.ShaderCompute);
				Device.ShaderCompute.SetUniformF("ellapsed", (float)RenderContext.Ellapsed);
				Device.ShaderCompute.SetUniformF("noiseOffset", (float)RenderContext.NoiseEllapsed);
				Device.ShaderCompute.SetUniformF("sizercp", _texDiffuseHistoryA.SizeReciprocal);
				Device.ShaderCompute.SetUniformF("finalSizercp", gbuffer.TexGbufferDepth.SizeReciprocal);
				Device.ShaderCompute.SetUniformI("resshift", ResShift);
				Device.BindImage2D(0, _texDiffuseRawA, TextureAccess.WriteOnly);
				Device.BindTexture(world.TexWorld, 0);
				Device.BindTexture(gbuffer.TexGbufferNormal, 1);
				Device.BindTexture(gbuffer.TexGbufferPosition, 2);
				Device.BindTexture(world.TexWorldMaterial, 3);
				Device.BindTexture(texBlueNoise, 4);
				Device.BindTexture(world.TexWorldLightmap, 5);
				Device.BindTexture(texSky, 6);
				Device.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 7, world.BufferPallete);
				Device.DispatchComputeThreads(_texDiffuseRawA.Width, _texDiffuseRawA.Height);

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);

				queryTrace.EndQuery();
				var queryAccum = Device.CreateStartQuery("Diffuse GI accumulate");

				//
				// Diffuse accumulate
				//
				Device.BindPipeline(_psoLightDiffuseAccumulate);
				camera.SetUniforms(Device.ShaderCompute);
				world.SetWorldSize(Device.ShaderCompute);
				Device.ShaderCompute.SetUniformF("cameraPlaneHistory", cameraHistory.Plane);
				Device.ShaderCompute.SetUniformF("sizercp", _texDiffuseHistoryA.SizeReciprocal);
				Device.ShaderCompute.SetUniformF("finalSizercp", gbuffer.TexGbufferDepth.SizeReciprocal);
				Device.ShaderCompute.SetUniformF("cameraMatrixHistory", cameraHistory.GetCameraProjectionMatrix());
				Device.ShaderCompute.SetUniformI("texsize", _texDiffuseHistoryA.Width, _texDiffuseHistoryA.Height);
				Device.ShaderCompute.SetUniformI("resshift", ResShift);
				cameraHistory.SetUniforms(Device.ShaderCompute, "History");
				Device.BindImage2D(0, _texDiffuseHistoryA, TextureAccess.WriteOnly);
				Device.BindTexture(gbuffer.TexGbufferPosition, 0);
				Device.BindTexture(gbuffer.TexGbufferNormal, 1);
				Device.BindTexture(_texDiffuseRawA, 2);
				Device.BindTexture(gbuffer.TexGbufferDepthHistory, 3);
				Device.BindTexture(gbuffer.TexGbufferNormalHistory, 4);
				Device.BindTexture(_texDiffuseHistoryB, 5);
				Device.BindTexture(world.TexWorldLightmap, 6);
				Device.BindTexture(world.TexWorld, 7);
				Device.DispatchComputeThreads(_texDiffuseHistoryA.Width, _texDiffuseHistoryA.Height);

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);

				queryAccum.EndQuery();
				var queryDenoise = Device.CreateStartQuery("Diffuse GI denoise");

				//
				// Diffuse denoise (iterative)
				//
				Device.BindPipeline(_psoLightDiffuseDenoise);
				camera.SetUniforms(Device.ShaderCompute);
				world.SetWorldSize(Device.ShaderCompute);
				Device.ShaderCompute.SetUniformF("sizercp", _texDiffuseRawA.SizeReciprocal);
				Device.ShaderCompute.SetUniformF("finalSizercp", gbuffer.TexGbufferDepth.SizeReciprocal);
				Device.ShaderCompute.SetUniformI("texsize", _texDiffuseRawA.Width, _texDiffuseRawA.Height);
				Device.ShaderCompute.SetUniformI("resshift", ResShift);
				Device.BindTexture(gbuffer.TexGbufferDepth, 0);
				Device.BindTexture(gbuffer.TexGbufferNormal, 1);

				for (int i = 0; i < 5; i++)
				{
					var tmpdifr = _texDiffuseRawA;
					_texDiffuseRawA = _texDiffuseRawB;
					_texDiffuseRawB = tmpdifr;
					var tmpstop = _texEdgeStopA;
					_texEdgeStopA = _texEdgeStopB;
					_texEdgeStopB = tmpstop;

					Device.BindTexture(_texEdgeStopB, 3);
					Device.ShaderCompute.SetUniformI("stepSize", 1 << i);
					Device.BindImage2D(0, _texDiffuseRawA, TextureAccess.WriteOnly);
					Device.BindImage2D(1, _texEdgeStopA, TextureAccess.WriteOnly);
					if (i == 0)
					{
						Device.BindTexture(_texDiffuseHistoryA, 2);
						Device.ShaderCompute.SetUniformI("useStopTex", 0);
					}
					else
					{
						Device.BindTexture(_texDiffuseRawB, 2);
						Device.ShaderCompute.SetUniformI("useStopTex", 1);
					}

					Device.DispatchComputeThreads(_texDiffuseRawA.Width, _texDiffuseRawA.Height);

					GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.TextureFetchBarrierBit);
				}

				queryDenoise.EndQuery();

				if(ResShift > 0)
				{
					upscaler.Upscale(gbuffer, camera, _texDiffuseRawA, ResShift, _texDiffuseFinal);
				}
			}

			queryAll.EndQuery();
		}
	}
}
