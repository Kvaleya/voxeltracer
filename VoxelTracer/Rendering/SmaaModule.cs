﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GlobCore;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat;
using Utils = GlobCore.Utils;

namespace VoxelTracer.Rendering
{
	class ModuleSmaa : RenderModule
	{
		GraphicsPipeline[] _psoSmaa;

		Texture2D _texEdge;
		Texture2D _texBlend;

		Texture2D _texArea;
		Texture2D _texSearch;

		FrameBuffer _fboEdge;
		FrameBuffer _fboBlend;

		public ModuleSmaa(RenderContext renderContext)
			: base(renderContext)
		{
			LoadTextures();

			_psoSmaa = new GraphicsPipeline[3];

			for (int i = 0; i < _psoSmaa.Length; i++)
			{
				string pass = (i == 0 ? "SMAA_PASS_EdgeDetection" : (i == 1 ? "SMAA_PASS_BlendingWeightCalculation" : "SMAA_PASS_NeighborhoodBlending"));

				_psoSmaa[i]?.Dispose();

				// TODO: breaks on changing resolution
				var defines = new List<Tuple<string, string>>()
				{
					new Tuple<string, string>(pass, "1"),
					new Tuple<string, string>(null, "uniform vec2 sizercp;"),
					new Tuple<string, string>(null, "#define SMAA_PIXEL_SIZE sizercp"),
					new Tuple<string, string>("SMAA_GLSL_3", "1"),
					new Tuple<string, string>("SMAA_PRESET_ULTRA", "1"),
				};

				if (i == 2)
				{
					//defines.Add(new Tuple<string, string>("pow4(a, b)", "vec4(pow(a.x, b), pow(a.y, b), pow(a.z, b), pow(a.w, b))"));
					//defines.Add(new Tuple<string, string>("SMAASampleLevelZero(tex, coord)", "pow4(textureLod(tex, coord, 0.0), 2.2)"));
				}

				var definesVS = defines.ToList();
				definesVS.Add(new Tuple<string, string>("SMAA_ONLY_COMPILE_VS", "1"));

				var definesPS = defines.ToList();
				definesPS.Add(new Tuple<string, string>("SMAA_ONLY_COMPILE_PS", "1"));

				_psoSmaa[i] = new GraphicsPipeline(Device, Device.GetShader("smaa.vert", definesVS), Device.GetShader("smaa.frag", definesPS), null, new RasterizerState(), new DepthState());
			}
		}

		public void Resize(int w, int h)
		{
			_fboEdge?.Dispose();
			_fboBlend?.Dispose();
			_texEdge?.Dispose();
			_texBlend?.Dispose();

			_texEdge = new Texture2D(Device, "SMAA-Edge", SizedInternalFormatGlob.RGBA8, w, h, 1);
			_texBlend = new Texture2D(Device, "SMAA-Blend", SizedInternalFormatGlob.RGBA8, w, h, 1);

			GlobCore.Utils.SetTextureParameters(Device, _texEdge, TextureWrapMode.ClampToEdge, TextureMagFilter.Linear, TextureMinFilter.Linear);
			GlobCore.Utils.SetTextureParameters(Device, _texBlend, TextureWrapMode.ClampToEdge, TextureMagFilter.Linear, TextureMinFilter.Linear);

			_fboEdge = new FrameBuffer();
			RenderContext.Device.BindFrameBuffer(_fboEdge, FramebufferTarget.Framebuffer);
			_fboEdge.Attach(FramebufferAttachment.ColorAttachment0, _texEdge);

			_fboBlend = new FrameBuffer();
			RenderContext.Device.BindFrameBuffer(_fboBlend, FramebufferTarget.Framebuffer);
			_fboBlend.Attach(FramebufferAttachment.ColorAttachment0, _texBlend);

			Utils.CheckFramebufferErrors(RenderContext.Device);

			Device.BindFrameBuffer(FrameBuffer.BackBuffer, FramebufferTarget.Framebuffer);
			Device.BindTexture(TextureTarget.Texture2D, 0, 0);
		}

		/// <summary>
		/// Replaces gbuffer's scenecolor texture with antialiased version
		/// </summary>
		/// <param name="gbuffer">Gbuffer module</param>
		public void Render(Texture2D texColor, Texture2D texDepth, FrameBuffer destination)
		{
			var querySmaa = Device.CreateStartQuery("smaa");

			using(Device.DebugMessageManager.PushGroupMarker("SMAA"))
			{
				Device.BindTexture(texColor, 0);
				Device.BindTexture(texDepth, 1);
				Device.BindTexture(_texEdge, 2);
				Device.BindTexture(_texBlend, 3);
				Device.BindTexture(_texArea, 4);
				Device.BindTexture(_texSearch, 5);

				OpenTK.Mathematics.Vector2 sizercp = new OpenTK.Mathematics.Vector2(1f / texColor.Width, 1f / texColor.Height);

				Device.BindFrameBuffer(_fboEdge, FramebufferTarget.Framebuffer);
				Device.BindPipeline(_psoSmaa[0]);
				Device.ShaderVertex.SetUniformF("sizercp", sizercp);
				Device.ShaderFragment.SetUniformF("sizercp", sizercp);
				GL.Clear(ClearBufferMask.ColorBufferBit);
				GL.DrawArrays(PrimitiveType.Triangles, 0, 3);

				Device.BindFrameBuffer(_fboBlend, FramebufferTarget.Framebuffer);
				Device.BindPipeline(_psoSmaa[1]);
				Device.ShaderVertex.SetUniformF("sizercp", sizercp);
				Device.ShaderFragment.SetUniformF("sizercp", sizercp);
				GL.Clear(ClearBufferMask.ColorBufferBit);
				GL.DrawArrays(PrimitiveType.Triangles, 0, 3);

				Device.BindFrameBuffer(destination, FramebufferTarget.Framebuffer);
				Device.BindPipeline(_psoSmaa[2]);
				Device.ShaderVertex.SetUniformF("sizercp", sizercp);
				Device.ShaderFragment.SetUniformF("sizercp", sizercp);
				GL.Clear(ClearBufferMask.ColorBufferBit);
				GL.DrawArrays(PrimitiveType.Triangles, 0, 3);
			}

			querySmaa.EndQuery();
		}

		void LoadTextures()
		{
			_texArea = new Texture2D(Device, "SMAA-Area", SizedInternalFormatGlob.RG8, 160, 560, 1);
			_texSearch = new Texture2D(Device, "SMAA-Search", SizedInternalFormatGlob.R8, 66, 33, 1);
			_texArea.TexSubImage2D(Device, 0, 0, 0, _texArea.Width, _texArea.Height, PixelFormat.Rg, PixelType.UnsignedByte, SmaaTextures.AreaTexBytes);
			//_texSearch.TexSubImage2D(Device, 0, 0, 0, _texSearch.Width, _texSearch.Height, PixelFormat.Red, PixelType.UnsignedByte, SmaaTextures.SearchTexBytes);

			// Loading the texture of weird size (66x33) directly produces weird results
			unsafe
			{
				fixed (byte* b = SmaaTextures.SearchTexBytes)
				{
					for(int y = 0; y < _texSearch.Height; y++)
					{
						_texSearch.TexSubImage2D(Device, 0, 0, y, _texSearch.Width, 1, PixelFormat.Red, PixelType.UnsignedByte, new IntPtr(b + y * _texSearch.Width));
					}
				}
			}

			GlobCore.Utils.SetTextureParameters(Device, _texArea, TextureWrapMode.ClampToEdge, TextureMagFilter.Linear, TextureMinFilter.Linear);
			GlobCore.Utils.SetTextureParameters(Device, _texSearch, TextureWrapMode.ClampToEdge, TextureMagFilter.Nearest, TextureMinFilter.Nearest);
		}
	}
}
