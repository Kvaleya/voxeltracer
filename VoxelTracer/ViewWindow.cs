﻿using System;
using System.IO;
using System.Threading;
using OpenTK.Windowing.Common;
using OpenTK.Windowing.Desktop;
using OpenTK.Graphics.OpenGL;
using System.Diagnostics;
using GlobCore;

namespace VoxelTracer
{
	class ViewWindow : GameWindow
	{
		public delegate void DrawFrame();
		public delegate void UpdateFrame();
		public delegate void Resize(int width, int height);

		public Device Device { get; private set; }

		private DrawFrame _onDrawFrame;
		private Resize _onResize;
		private UpdateFrame _onUpdate;
		private TextOutput _textOutput;

		public bool MouseLocked
		{
			get
			{
				return !this.CursorVisible;
			}
			set
			{
				if (value == this.CursorVisible)
				{
					this.CursorVisible = !value;
					this.CursorGrabbed = value;
				}
			}
		}

		public ViewWindow(DrawFrame onDrawFrame, Resize onResize, UpdateFrame update, TextOutput textOutput)
			: base(new GameWindowSettings()
			{
				IsMultiThreaded = false,
				RenderFrequency = 0,
				UpdateFrequency = 240,
			},  new NativeWindowSettings()
			{
				API = ContextAPI.OpenGL,
				APIVersion = new Version(4, 3),
				Flags = ContextFlags.Debug,
			})
		{
			Context.MakeCurrent();
			this.IsEventDriven = false;
			_onUpdate = update;
			_onDrawFrame = onDrawFrame;
			_onResize = onResize;
			_textOutput = textOutput;

			this.VSync = VSyncMode.Off;

			this.Title = "Voxel thingy";

			string shadersPath = Path.Combine(Environment.CurrentDirectory, "Shaders");

			Device = new Device((type, message) =>
			{
				if (type != OutputTypeGlob.LogOnly)
					_textOutput.OnMessage(type.ToString() + ": " + message); // TODO: multiple message types from glob
			}, filename =>
			{
				const int tries = 10;

				for (int i = 0; i < tries; i++)
				{
					try
					{
						return File.OpenRead(Path.Combine(shadersPath, filename));
					}
					catch (IOException e)
					{
						if (i == tries - 1)
							throw e;
						// File is probably in use by another process - the shader editor
						// wait for it to finish saving the file
						Thread.Sleep(50);
					}
				}

				return null;
			});
			DebugMessageManager.SetupDebugCallback(Device);
			Device.StartShaderFileWatcher(shadersPath);
			//GL.ClipControl(ClipOrigin.LowerLeft, ClipDepthMode.ZeroToOne);
			GL.Enable(EnableCap.DepthTest);
			GL.Enable(EnableCap.StencilTest);
			GL.Enable(EnableCap.TextureCubeMapSeamless);
		}

		Stopwatch _sw = new Stopwatch();

		public double LastTimed = 0;
		public double LastTimedWithSwap = 0;

		protected override void OnRenderFrame(FrameEventArgs e)
		{
			_sw.Restart();
			base.OnRenderFrame(e);
			Device.Update();
			Device.Invalidate();

			GL.Viewport(0, 0, this.Size.X, this.Size.Y);
			GL.ClearColor(0f, 0, 0, 0);
			GL.ClearDepth(1);
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit | ClearBufferMask.StencilBufferBit);

			_onDrawFrame?.Invoke();
			GL.Flush();

			LastTimed = _sw.ElapsedTicks / (double)Stopwatch.Frequency;

			SwapBuffers();

			LastTimedWithSwap = _sw.ElapsedTicks / (double)Stopwatch.Frequency;
		}

		protected override void OnUpdateFrame(FrameEventArgs args)
		{
			base.OnUpdateFrame(args);
			_onUpdate?.Invoke();
		}

		protected override void OnResize(ResizeEventArgs e)
		{
			base.OnResize(e);
			_onResize.Invoke(e.Width, e.Height);
		}
	}
}
