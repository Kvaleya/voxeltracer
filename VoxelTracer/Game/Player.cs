﻿using System;
using System.Collections.Generic;
using System.Text;
using VoxelTracer.Rendering;
using OpenTK.Mathematics;
using OpenTK.Windowing.GraphicsLibraryFramework;

namespace VoxelTracer.Game
{
	enum PlayerControlState
	{
		FlightNoClip,
		Flight,
		Walk,
	}

	class Player
	{
		Game.World _world;

		// Camera
		public Vector3 Position = new Vector3(32, 50, -8);
		public float HorizontalFov = 100f; // In degrees
		public double Pitch = 0, Yaw = 0; // In radians

		// Positive yaw = looking right
		public double YawDegrees { get { return Yaw * 180 / Math.PI; } set { Yaw = value / 180 * Math.PI; } }
		// Positive pitch = looking up
		public double PitchDegrees { get { return Pitch * 180 / Math.PI; } set { Pitch = value / 180 * Math.PI; } }

		public Vector2 MouseSensitivity = new Vector2(0.1f);

		public Vector3 Forward
		{
			get
			{
				return new Vector3(
					(float)(Math.Sin(-Yaw) * Math.Cos(Pitch)),
					(float)(Math.Sin(Pitch)),
					(float)(Math.Cos(Yaw) * Math.Cos(Pitch))
				);
			}
		}

		public Vector3 GroundPlaneForward
		{
			get
			{
				return new Vector3(
					(float)Math.Sin(-Yaw),
					0,
					(float)Math.Cos(Yaw)
				);
			}
		}

		public Vector3 Right { get { return Vector3.Cross(Forward, Vector3.UnitY).Normalized(); } }

		public Camera GetCamera(int width, int height)
		{
			return new Camera(Position + Vector3.UnitY * (_radius.Y * 0.9f) + _cameraPositionDelta, HorizontalFov, width / (float)height, Forward);
		}

		// Keybindings
		public Keys KeyForward = Keys.W, KeyBack = Keys.S, KeyLeft = Keys.A,
			KeyRight = Keys.D, KeyUp = Keys.Space,
			KeySprint = Keys.LeftShift, KeyWalk = Keys.LeftAlt,
			KeyFlightToggle = Keys.C;

		// Player collision and physics
		Vector3 _radius = new Vector3(0.8f, 1.4f, 0.8f);
		// Player will never be closer than this value to a wall
		// too large values make stair climbing impossible
		// since the player would not be able to cover the epsilon distance in a single step,
		// thus never reaching the stair edge
		const float VoxelEpsilon = 0.001f;
		public PlayerControlState ControlState = PlayerControlState.FlightNoClip;
		float _upVelocity = 0f;
		bool _onGround = false;

		Vector3 _cameraPositionDelta = Vector3.Zero;

		public float FlySpeed = 16.0f;
		public float WalkSpeed = 8.0f;
		public float FlyMultiplier = 6;
		public float WalkMultiplier = 2;

		// One unit is one voxel
		// but the player is ~4 voxels high
		// so boost gravity so that it feels sort of natural
		const float Gravity = -9.8f * 4.0f;
		const float Jump = 15f;

		public Player(Game.World world)
		{
			_world = world;
		}

		// TODO: setrvačnost když je hráč ve vzduchu / air control
		// TODO: citlivost myši

		public void UpdateControlsFPS(InputState input, double deltaTime)
		{
			// Camera look
			YawDegrees += input.Mouse.Delta.X * MouseSensitivity.X;
			PitchDegrees -= input.Mouse.Delta.Y * MouseSensitivity.Y;

			while (Yaw < 0)
				Yaw += Math.PI * 2;
			while (Yaw > Math.PI * 2)
				Yaw -= Math.PI * 2;

			double pitchLimit = Math.PI * 0.49;

			if (Pitch > pitchLimit)
				Pitch = pitchLimit;
			if (Pitch < -pitchLimit)
				Pitch = -pitchLimit;

			// Camera movement
			Vector3 move = Vector3.Zero;

			if (input.Keyboard.IsKeyDown(KeyForward))
				move.Z += 1;
			if (input.Keyboard.IsKeyDown(KeyBack))
				move.Z -= 1;
			if (input.Keyboard.IsKeyDown(KeyRight))
				move.X += 1;
			if (input.Keyboard.IsKeyDown(KeyLeft))
				move.X -= 1;
			if (input.Keyboard.IsKeyDown(KeyUp))
				move.Y += 1;

			float speed = ControlState == PlayerControlState.Walk ? WalkSpeed : FlySpeed;
			float multiplier = ControlState == PlayerControlState.Walk ? WalkMultiplier : FlyMultiplier;

			if (input.Keyboard.IsKeyDown(KeySprint))
				move *= multiplier;
			if (input.Keyboard.IsKeyDown(KeyWalk))
				move /= multiplier;

			if (input.Keyboard.IsKeyDown(KeyFlightToggle) && !input.Keyboard.WasKeyDown(KeyFlightToggle))
				ControlState = ControlState == Game.PlayerControlState.Walk ? Game.PlayerControlState.FlightNoClip : Game.PlayerControlState.Walk;

			move *= (float)deltaTime * speed;

			Vector3 delta = Vector3.Zero;

			// Generate movement delta
			if(ControlState == PlayerControlState.Walk)
			{
				delta += GroundPlaneForward * move.Z;
				delta += Right * move.X;
				if (_onGround && move.Y > 0)
				{
					_onGround = false;
					_upVelocity = Math.Max(Jump, _upVelocity);
				}
			}
			else
			{
				delta += Forward * move.Z;
				delta += Right * move.X;
				delta += Vector3.UnitY * move.Y;
			}


			if (ControlState == PlayerControlState.FlightNoClip)
			{
				// Noclip flight: just move in the desired direction
				Position += delta;
			}
			if (ControlState == PlayerControlState.Flight)
			{
				// Collision flight: move and slide along walls
				TryMove(Position, delta, out Position);
			}
			if (ControlState == PlayerControlState.Walk)
			{
				// Regular walking: move, fall, jump, walk up stairs
				Vector3 resultWalk;
				TryMoveStairs(Position, delta, out resultWalk);

				// Gravity
				float dt = (float)deltaTime;
				float moveUp = _upVelocity * dt + Gravity * 0.5f * dt * dt;
				_upVelocity += (float)deltaTime * Gravity;

				// Handle going down stairs smoothly

				Vector3 resultGravity;
				bool fallOnGround = TryMove(resultWalk, new Vector3(0, moveUp, 0), out resultGravity);

				if(!fallOnGround && _onGround)
				{
					// We lost ground beneath our feet, try to snap to groud (walking down stairs like)
					Vector3 resultSnap;
					bool snapToGround = TryMove(resultWalk, new Vector3(0, -1.2f, 0), out resultSnap);

					if(snapToGround)
					{
						Position = resultSnap;
						_cameraPositionDelta += new Vector3(0, resultWalk.Y - resultSnap.Y, 0);
					}
					else
					{
						_onGround = false;
						Position = resultGravity;
					}
				}
				else
				{
					Position = resultGravity;
				}

				if (fallOnGround)
				{
					_upVelocity = 0;
					_onGround = true;
				}
			}
			else
			{
				_upVelocity = 0;
				_onGround = false;
			}

			_cameraPositionDelta = _cameraPositionDelta * Math.Max(0f, 1.0f - (float)deltaTime * 20.0f);
			if (_cameraPositionDelta.Length > 1f)
				_cameraPositionDelta.Normalize();
		}

		void TryMoveStairs(Vector3 origin, Vector3 delta, out Vector3 result)
		{
			result = origin;
			Vector3 localResult;
			bool collided = TryMove(origin, delta, out localResult);

			// Try to move up stairs
			if (collided && _onGround)
			{
				Vector3 stairsResult;
				bool stairCollided = TryMove(origin + new Vector3(0, 1.5f, 0), delta, out stairsResult);
				Vector3 stairsDown;
				bool stairsOnGround = TryMove(stairsResult, new Vector3(0, -1.0f, 0), out stairsDown);

				if (stairsOnGround && stairsDown.Y > localResult.Y)
				{
					result = stairsDown;
					// Animate camera movement
					_cameraPositionDelta += new Vector3(0, localResult.Y - stairsDown.Y, 0);
				}
				else
				{
					result = localResult;
				}
			}
			else
			{
				result = localResult;
			}
		}

		bool TryMove(Vector3 origin, Vector3 delta, out Vector3 result)
		{
			result = origin;
			bool collided = false;
			float len = delta.Length;
			Vector3 normal = delta / len;
			float traversed = 0;
			while(traversed < len)
			{
				Vector3 localResult;
				Vector3 direction = normal * Math.Min(1, len - traversed);
				collided = TryMoveOneStep(result, direction, out localResult) | collided;
				result = localResult;
				traversed += 1;
			}

			return collided;
		}

		// Delta must be smaller than one voxel in each axis
		bool TryMoveOneStep(Vector3 origin, Vector3 delta, out Vector3 result)
		{
			// AABB of current player position
			Vector3i voxelMin = (origin - _radius).ToIvecFloor();
			Vector3i voxelMax = (origin + _radius).ToIvecFloor();

			result = origin;

			bool anycollision = false;

			for (int axis = 0; axis < 3; axis++)
			{
				Vector3i searchStart = voxelMin;
				Vector3i searchStop = voxelMax;

				if (Math.Abs(delta[axis]) == 0f)
					continue;

				bool positive = delta[axis] >= 0;
				int axisPos = positive ? (voxelMax[axis] + 1) : (voxelMin[axis] - 1);

				searchStart[axis] = axisPos;
				searchStop[axis] = axisPos;
				
				// Causes wall climbing
				/*
				for(int i = 0; i < 3; i++)
				{
					if (i == axis)
						continue;
					searchStart[i] = (int)Math.Floor(origin[i] - _radius[i] - VoxelEpsilon);
					searchStop[i] = (int)Math.Floor(origin[i] + _radius[i] + VoxelEpsilon);
				}
				*/
				bool collided = false;

				for (int z = searchStart[2]; z <= searchStop[2]; z++)
				{
					for (int y = searchStart[1]; y <= searchStop[1]; y++)
					{
						for (int x = searchStart[0]; x <= searchStop[0]; x++)
						{
							if (_world[x, y, z])
							{
								collided = true;
								break;
							}
						}
					}
				}

				if(collided)
				{
					float uncollidedPos = origin[axis] + delta[axis];
					if (positive)
					{
						float collidedPos = axisPos - _radius[axis] - VoxelEpsilon;
						if(collidedPos < uncollidedPos)
						{
							anycollision = true;
							result[axis] = collidedPos;
						}
						else
						{
							result[axis] = uncollidedPos;
						}
					}
					else
					{
						float collidedPos = axisPos + 1 + _radius[axis] + VoxelEpsilon;
						if (collidedPos > uncollidedPos)
						{
							anycollision = true;
							result[axis] = collidedPos;
						}
						else
						{
							result[axis] = uncollidedPos;
						}
					}
				}
				else
				{
					result[axis] += delta[axis];
				}
			}

			return anycollision;
		}
	}
}
