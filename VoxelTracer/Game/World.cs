﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VoxelTracer.Game
{
	class World
	{
		public int WorldSizePow { get; private set; } = 7;
		public int WorldSize => 1 << WorldSizePow;

		// Collision only, returns false if out of bounds
		public bool this[int x, int y, int z]
		{
			get
			{
				if (x < 0 || y < 0 || z < 0 || x >= WorldSize || y >= WorldSize || z >= WorldSize)
					return false;
				return WorldCollisions[x + y * WorldSize + z * WorldSize * WorldSize] == 0;
			}
		}

		public byte[] WorldCollisions = null;
		public byte[] WorldMaterials = null;

		public uint[] PalleteBuffer = new uint[1];

		public void Load(string file, int sizepow)
		{
			WorldSizePow = sizepow;
			/*/
			Random random = new Random(42);
			WorldCollisions = new byte[WorldSize * WorldSize * WorldSize];
			WorldMaterials = new byte[WorldSize * WorldSize * WorldSize];

			for (int i = 0; i < world.Length; i++)
			{
				int x = i % WorldSize;
				int y = (i / WorldSize) % WorldSize;
				int z = i / WorldSize / WorldSize;

				bool col = (random.NextDouble() < 0.1);
				world[i] = (byte)(col ? 0 : 1);
				worldMat[i] = 0;
				if(col)
				{
					if(random.NextDouble() < 0.0005)
						worldMat[i] = (byte)(random.Next(2) + 1);
					if (random.NextDouble() < 0.15)
						worldMat[i] = (byte)(random.Next(4) + 3);
				}

				if (x == WorldSize / 2 && y == WorldSize / 2)
				{
					world[i] = 0;
					worldMat[i] = 255;
				}
			}
			/*/
			VoxLoader.LoadVox(new System.IO.BinaryReader(System.IO.File.OpenRead(file)), WorldSize,
				out WorldCollisions, out PalleteBuffer, out WorldMaterials);
			/**/
		}
	}
}
