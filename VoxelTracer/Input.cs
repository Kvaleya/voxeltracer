﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK;
using OpenTK.Windowing.GraphicsLibraryFramework;
using OpenTK.Mathematics;
using OpenTK.Windowing.Desktop;

namespace VoxelTracer
{
	class InputState
	{
		public KeyboardState Keyboard;
		public MouseState Mouse;
	}
}
