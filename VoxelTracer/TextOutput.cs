﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VoxelTracer
{
	class TextOutput
	{
		public void OnError(string message)
		{
			Console.WriteLine(message);
		}

		public void OnMessage(string message)
		{
			Console.WriteLine(message);
		}
	}
}
