﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;
using System.Linq;
using System.Globalization;
using OpenTK.Mathematics;

namespace VoxelTracer
{
	class MainLoop
	{
		public TextOutput TextOutput;
		public ViewWindow Window;
		public WorldRenderer Renderer;
		public ImGuiRenderer ImGui;

		public static MainLoop Instance;
		public Game.World World = new Game.World();

		Stopwatch _stopwatch = new Stopwatch();
		double _lastEllapsed = 0;
		double _currentEllapsed = 0;
		double _gameEllapsed = 0;
		double _currentDeltaT = 0;
		private double GetEllapsed() { return _stopwatch.ElapsedTicks / (double)Stopwatch.Frequency; }
		//Input _input = null;

		long _frame = 0;
		double[] _lastDeltas = new double[64];

		public Game.Player Player;

		public void Start()
		{
			Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;
			TextOutput = new TextOutput();

			Window = new ViewWindow(() => this.UpdateAndDrawFrame(), (w, h) => this.Renderer.OnResize(w, h),
				null, TextOutput);
			Window.MouseLocked = true;
			//_input = new Input(Window);
			Renderer = new WorldRenderer(Window.Device);
			LoadWorld();
			Player = new Game.Player(World);

			ImGui = new ImGuiRenderer(Window, Window.Device);
			ImGui.RebuildFontAtlas();

			Window.Size = new Vector2i(1280, 720);
			_stopwatch.Start();
			Window.Run();
		}

		double[] GetLastDeltas(int count)
		{
			int localDelta = (int)(_frame % _lastDeltas.Length) + _lastDeltas.Length * (count / _lastDeltas.Length + 2);

			double[] deltas = new double[count];

			for(int i = 0; i < count; i++)
			{
				int index = (localDelta - i) % _lastDeltas.Length;
				deltas[i] = _lastDeltas[index];
			}

			return deltas;
		}

		void UpdateAndDrawFrame()
		{
			_currentEllapsed = GetEllapsed();
			_currentDeltaT = _currentEllapsed - _lastEllapsed;
			_lastEllapsed = _currentEllapsed;
			_lastDeltas[_frame % _lastDeltas.Length] = _currentDeltaT;

			this.Window.Title = "Voxel thingy " + Math.Round(_lastDeltas.Average() * 1000, 1) + "ms";

			var lastFrames = GetLastDeltas(16);
			double usableDelta = _currentDeltaT;
			_gameEllapsed = _currentEllapsed;

			var inputState = new InputState()
			{
				Keyboard = Window.KeyboardState,
				Mouse = Window.MouseState,
			};
			UpdateControls(inputState, usableDelta);

			ImGui.NewFrame();

			Renderer.DrawFrame(usableDelta, _gameEllapsed, _currentDeltaT, Player.GetCamera(Window.Size.X, Window.Size.Y));

			// Call BeforeLayout first to set things up
			ImGui.BeforeLayout((float)_currentDeltaT);

			// Draw our UI
			UserInterface.Draw(Renderer.Device, ImGui, Renderer, _lastDeltas);

			// Call AfterLayout now to finish up and draw all the things
			ImGui.AfterLayout();

			_frame++;
		}

		static readonly string[] _levels =
		{
			"Vox/monu1.vox",
			"Vox/monu2.vox",
			"Vox/monu3.vox",
			"Vox/monu4.vox",
			"Vox/monu5.vox",
			"Vox/monu6.vox",
			"Vox/monu7.vox",
			"Vox/monu8_mod.vox",
			"Vox/monu9.vox",
			"Vox/monu10.vox",
			"Vox/monu16.vox",
			"Vox/teapot.vox",
			"Vox/nature.vox",
		};

		void UpdateControls(InputState input, double deltaTime)
		{
			// Camera lock/unlock
			if (input.Keyboard.IsKeyDown(OpenTK.Windowing.GraphicsLibraryFramework.Keys.Space) &&
				!input.Keyboard.WasKeyDown(OpenTK.Windowing.GraphicsLibraryFramework.Keys.Space) &&
				Player.ControlState != Game.PlayerControlState.Walk)
			{
				Window.MouseLocked = !Window.MouseLocked;
			}

			if(input.Keyboard.IsKeyDown(OpenTK.Windowing.GraphicsLibraryFramework.Keys.I))
			{
				Renderer.Context.SunAngle += deltaTime;
			}
			if (input.Keyboard.IsKeyDown(OpenTK.Windowing.GraphicsLibraryFramework.Keys.O))
			{
				Renderer.Context.SunAngle -= deltaTime;
			}
			if (input.Keyboard.IsKeyDown(OpenTK.Windowing.GraphicsLibraryFramework.Keys.L) &&
				!input.Keyboard.WasKeyDown(OpenTK.Windowing.GraphicsLibraryFramework.Keys.L))
			{
				Renderer.Context.LevelIndex++;
				LoadWorld();
			}
			if (input.Keyboard.IsKeyDown(OpenTK.Windowing.GraphicsLibraryFramework.Keys.K) &&
				!input.Keyboard.WasKeyDown(OpenTK.Windowing.GraphicsLibraryFramework.Keys.K))
			{
				Renderer.QualityPreset++;
			}

			if (Window.MouseLocked)
				Player.UpdateControlsFPS(input, deltaTime);
		}

		void LoadWorld()
		{
			World.Load(_levels[Renderer.Context.LevelIndex % _levels.Length], 7);
			Renderer.OnUpdateWorld(World);
		}
	}
}
