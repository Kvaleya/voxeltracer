#version 430

layout(location = 0) out vec4 outColor;

layout(binding = 0) uniform sampler2D texColor;
layout(binding = 1) uniform sampler2D texDepth;
layout(binding = 2) uniform sampler2D texEdge;
layout(binding = 3) uniform sampler2D texBlend;
layout(binding = 4) uniform sampler2D texArea;
layout(binding = 5) uniform sampler2D texSearch;

#include SMAA.h

#ifdef SMAA_PASS_NeighborhoodBlending
#define OFFSET_COUNT 2
#else
#define OFFSET_COUNT 3
#endif

layout(location = 0) in vec2 v_texcoord;
layout(location = 1) in vec2 v_pixcoord;
layout(location = 2) in vec4 v_offset0;
layout(location = 3) in vec4 v_offset1;
layout(location = 4) in vec4 v_offset2;

void main()
{
	#ifdef SMAA_PASS_NeighborhoodBlending
	vec4 v_offset[2];
	v_offset[0] = v_offset0;
	v_offset[1] = v_offset1;
	#else
	vec4 v_offset[3];
	v_offset[0] = v_offset0;
	v_offset[1] = v_offset1;
	v_offset[2] = v_offset2;
	#endif

	#ifdef SMAA_PASS_EdgeDetection
		#if SMAA_PREDICATION == 1
		outColor = SMAALumaEdgeDetectionPS(v_texcoord, v_offset, texColor, texDepth);
		#else
		outColor = SMAALumaEdgeDetectionPS(v_texcoord, v_offset, texColor);
		#endif
	#endif
	
	#ifdef SMAA_PASS_BlendingWeightCalculation
	outColor = SMAABlendingWeightCalculationPS(v_texcoord, v_pixcoord, v_offset, texEdge, texArea, texSearch, ivec4(0));
	#endif
	
	#ifdef SMAA_PASS_NeighborhoodBlending
	outColor = SMAANeighborhoodBlendingPS(v_texcoord, v_offset, texColor, texBlend);
	#endif
}
