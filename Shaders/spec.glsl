#include world.glsl

uvec2 packRayParams(vec3 direction, float pdf)
{
	uvec2 u;
	vec2 s = toSpherical(direction);
	uvec2 d = uvec2(s * 65535);
	u.x = (d.x & 0xffff) << 0;
	u.x |= (d.y & 0xffff) << 16;
	u.y = floatBitsToUint(pdf);
	return u;
}

void unpackRayParams(uvec2 u, out vec3 direction, out float pdf)
{
	vec2 s;
	s.x = float((u.x >> 0) & 0xffff) / 65535.0;
	s.y = float((u.x >> 16) & 0xffff) / 65535.0;
	direction = fromSpherical(s);
	pdf = uintBitsToFloat(u.y);
}
