#version 430

out gl_PerVertex {
    vec4 gl_Position;
};

#include SMAA.h

#ifdef SMAA_PASS_NeighborhoodBlending
#define OFFSET_COUNT 2
#else
#define OFFSET_COUNT 3
#endif

layout(location = 0) out vec2 v_texcoord;
layout(location = 1) out vec2 v_pixcoord;
layout(location = 2) out vec4 v_offset0;
layout(location = 3) out vec4 v_offset1;
layout(location = 4) out vec4 v_offset2;

vec2 positions[3] = vec2[3](
	vec2(-1.0, -1.0),
	vec2(3.0, -1.0),
	vec2(-1.0, 3.0)
);

vec2 texCoords[3] = vec2[3](
	vec2(0.0, 0.0),
	vec2(2.0, 0.0),
	vec2(0.0, 2.0)
);

void main() {
	v_texcoord = texCoords[gl_VertexID].xy;
	vec4 pos = vec4(positions[gl_VertexID], 0.0, 1.0);

	#ifdef SMAA_PASS_NeighborhoodBlending
	vec4 v_offset[2];
	#else
	vec4 v_offset[3];
	#endif
	
	#ifdef SMAA_PASS_EdgeDetection
	SMAAEdgeDetectionVS(pos, gl_Position, v_texcoord, v_offset);
	#endif
	
	#ifdef SMAA_PASS_BlendingWeightCalculation
	SMAABlendingWeightCalculationVS(pos, gl_Position, v_texcoord, v_pixcoord, v_offset);
	#endif
	
	#ifdef SMAA_PASS_NeighborhoodBlending
	SMAANeighborhoodBlendingVS(pos, gl_Position, v_texcoord, v_offset);
	#endif
	
	#ifdef SMAA_PASS_NeighborhoodBlending
	v_offset0 = v_offset[0];
	v_offset1 = v_offset[1];
	#else
	v_offset0 = v_offset[0];
	v_offset1 = v_offset[1];
	v_offset2 = v_offset[2];
	#endif
}
