#version 430

layout(location = 0) in vec2 vertexPosition;
layout(location = 1) in vec2 vertexTexCoord;
layout(location = 2) in vec4 vertexColor;

uniform vec4 offset_scale;

out gl_PerVertex {
    vec4 gl_Position;
};

layout(location = 0) out vec2 vTexCoord;
layout(location = 1) out vec4 vColor;

void main() {
	vTexCoord = vertexTexCoord;
	vColor = vertexColor;
    gl_Position = vec4(vertexPosition * offset_scale.zw + offset_scale.xy, 0.0, 1.0);
}
