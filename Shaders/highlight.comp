#version 430

layout (binding = 0, rgba16f) uniform restrict writeonly image2D texOutput;

#include world.glsl
#include raytrace.glsl

layout(binding = 0) uniform usampler2D texGbufferPos;

// Both inclusive
uniform ivec3 selectionMin;
uniform ivec3 selectionMax;
uniform int selectionFace;
uniform vec2 sizercp;

uniform vec3 ray00;
uniform vec3 ray01;
uniform vec3 ray10;
uniform vec3 ray11;
uniform vec3 cameraPos;

vec3 GetRay(vec2 tc)
{
	return mix(
		mix(ray01, ray11, tc.x),
		mix(ray00, ray10, tc.x),
		1.0 - tc.y);
}

bool inRange(ivec3 vmin, ivec3 vmax, ivec3 p)
{
	for(int i = 0; i < 3; i++)
	{
		if(p[i] < vmin[i] || p[i] > vmax[i])
			return false;
	}

	return true;
}

void analyze(vec3 pos, float depth, int face, float thicknessMult, out bool edge, out bool outerEdge)
{
	float thickness = clamp(depth * 0.004, 0.01, 1.0) * thicknessMult;
	
	vec3 invoxel = fract(pos);
	
	float edgeDist = 1.0;
	
	outerEdge = false;
	int outerFaces = 0;
	
	for(int i = 0; i < 3; i++)
	{
		outerEdge = outerEdge || ((abs(selectionMin[i] - pos[i]) < thickness) || (abs(selectionMax[i] + 1 - pos[i]) < thickness));
		if((face >> 1) == i)
			continue;
		if(abs(pos[i] - selectionMin[i]) < 0.5 || abs(pos[i] - selectionMax[i] - 1) < 0.5)
			outerFaces++;
		edgeDist = min(edgeDist, min(invoxel[i], 1.0 - invoxel[i]));
	}

	outerEdge = outerEdge && (outerFaces > 0);
	
	if(!outerEdge)
		thickness = clamp(depth * 0.002, 0.01, 1.0) * thicknessMult;
	
	edge = edgeDist < thickness;
}

int boxFace(vec3 pos)
{
	for(int i = 0; i < 3; i++)
	{
		const float eps = 0.01;
		if(abs(pos[i] - selectionMin[i]) < eps)
			return i << 1;
		if(abs(pos[i] - selectionMax[i] - 1) < eps)
			return (i << 1) + 1;
	}
	return 5;
}

layout (local_size_x = 8, local_size_y = 8, local_size_z = 1) in;
void main()
{
	ivec2 pixelPos = ivec2(gl_GlobalInvocationID.xy);
	
	vec3 pos;
	float depth;
	ivec3 voxel;
	int face;
	vec3 view;
	unpackPositionGbuffer(texelFetch(texGbufferPos, pixelPos, 0), voxel, face, pos, depth, view);
	
	vec3 dir = GetRay(pixelPos * sizercp);
	float t1, t2;
	bool box = BBoxIntersect(vec3(selectionMin), vec3(selectionMax + 1), cameraPos, 1.0 / dir, t1, t2);
	
	if(!box)
		return;
	
	vec3 boxPos1 = cameraPos + dir * t1;
	vec3 boxPos2 = cameraPos + dir * t2;
	
	bool voxelEdge;
	bool voxelOuterEdge;
	analyze(pos, depth, face, 1.0, voxelEdge, voxelOuterEdge);
	
	const float boxThicknessMult = 0.75;
	
	bool box1Edge;
	bool box1OuterEdge;
	int box1Face = boxFace(boxPos1);
	analyze(boxPos1, t1, box1Face, boxThicknessMult, box1Edge, box1OuterEdge);
	
	bool box2Edge;
	bool box2OuterEdge;
	int box2Face = boxFace(boxPos2);
	analyze(boxPos2, t2, box2Face, boxThicknessMult, box2Edge, box2OuterEdge);
	
	bool draw = false;
	vec3 color = vec3(0.0);
	
	const vec3 col_box_edge_hidden = vec3(0.5, 0.25, 0.25);
	const vec3 col_box_edge = vec3(1.0, 0.25, 0.25);
	const vec3 col_fill = vec3(0.5);
	const vec3 col_face = color = vec3(0.5, 0.5, 1.0);
	
	if(box2Edge && t2 > 0.0 && depth > t2)
	{
		draw = true;
		color = box2Face == selectionFace ? col_face : col_fill;
	}
	
	if(box2OuterEdge && box2Edge && t2 > 0.0)
	{
		draw = true;
		color = depth > t2 ? col_box_edge : col_box_edge_hidden;
	}
	
	if(inRange(selectionMin, selectionMax, voxel) && voxelEdge)
	{
		draw = true;
		color = col_fill;
		if(voxelOuterEdge)
			color = col_box_edge;
	}
	
	if(box1Edge && t1 > 0.0 && depth > t1)
	{
		draw = true;
		color = box1Face == selectionFace ? col_face : col_fill;
	}
	
	if(box1OuterEdge && box1Edge && t1 > 0.0)
	{
		draw = true;
		color = depth > t1 ? col_box_edge : col_box_edge_hidden;
	}
	
	if(!draw)
		return;
	
	imageStore(texOutput, pixelPos, vec4(color, 1.0));
}
