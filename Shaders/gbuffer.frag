#version 430

layout(location = 0) out vec4 outColor;
layout(location = 1) out float outDepth;
layout(location = 2) out vec4 outNormal;
layout(location = 3) out vec4 outGlow;
layout(location = 4) out uvec4 outPos;

layout(location = 0) in vec2 screenPos;

#include raytrace.glsl
#include world.glsl

uniform float ellapsed;
uniform vec3 ray00;
uniform vec3 ray01;
uniform vec3 ray10;
uniform vec3 ray11;
uniform vec3 cameraPos;
uniform ivec3 worldSize;

layout(binding = 0) uniform usampler3D texWorld;
layout(binding = 1) uniform usampler3D texWorldMat;
layout(binding = 2) uniform samplerCube texSky;

vec3 GetRay(vec2 tc)
{
	return mix(
		mix(ray01, ray11, tc.x),
		mix(ray00, ray10, tc.x),
		1.0 - tc.y);
}

void main()
{
	vec3 dir = GetRay(screenPos);
	rayhit hit;
	traceRay(cameraPos, dir, texWorld, texWorldMat, worldSize, hit, ellapsed);
	material m = getWorldMaterial(texWorldMat, hit.voxel, hit.pos, ellapsed);
	
	outDepth = hit.t;
	
	outPos = packPositionGbuffer(hit.voxel, toFace(hit.normal), hit.pos, outDepth, normalize(-hit.dir));
	
	outColor = vec4(m.albedoNonlinear, 1.0);
	outNormal = vec4(hit.normal * 0.5 + 0.5, 0.0);
	outGlow = vec4(m.emmisive, 0.0);
	
	if(outDepth >= 1e9)
	{
		outColor = vec4(0.0, 0.0, 0.0, 1.0);
		outNormal = vec4(-dir, 0.0);
		outGlow = vec4(texture(texSky, normalize(hit.dir)).rgb, 0.0);
	}
}
