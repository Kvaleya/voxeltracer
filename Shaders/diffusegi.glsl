#include raytrace.glsl
#include world.glsl

#ifndef PI
#define PI 3.1415926535897932384626433832795
#endif

vec3 random3(vec3 st)
{
    vec3 s = vec3(dot(st, vec3(12.3456, 34.1415, 42.178)),
                  dot(st, vec3(42.2154, 15.2854, 21.457)),
    			  dot(st, vec3(11.4738, 25.8997, 37.889)));
    return fract(sin(s) * 45678.9) * 2.0 - 1.0;
}

// Adapted from https://rasmusbarr.github.io/blog/realtimegi-pt2.html#snippet
vec3 tangent(vec3 n) {
	vec3 t = abs(n.x) > 0.1 ? vec3(-n.z, 0, n.x) : vec3(0, -n.z, n.y);
	return normalize(t);
}
vec3 cosine_weighted_direction(vec3 n, float x, float y) {
	float a = 2*PI*x;
	vec3 u = tangent(n);
	vec3 v = cross(n, u);
	return (u*cos(a) + v*sin(a))*sqrt(y) + n*sqrt(1-y);
}

vec3 diffuseRayGen(sampler2D texBlueNoiseRayGen, ivec2 pixel, float noise_offset, int offset, vec3 normal, out float pdf)
{
	//vec2 r = random3(seed).xy * 0.5 + 0.5;
	vec2 r = texelFetch(texBlueNoiseRayGen, (ivec2(pixel + ivec2(fract(noise_offset * 21.4) * 1000.0, fract(noise_offset * 14.38) * 1247.0) + offset)) & 127, 0).xy;
	vec3 dir = cosine_weighted_direction(normal, r.x, r.y);
	
	pdf = 1.0 / (2.0 * PI) / max(0.0001, dot(dir, normal));
	return dir;
}

#define nanguard(v) v = isnan(v.x) ? vec3(0.0) : v

vec3 pathTraceGi(usampler3D tex_world, usampler3D tex_material, sampler3D tex_lightmap, samplerCube tex_sky, ivec3 world_size, float time_ellapsed, sampler2D tex_bluenoise, ivec2 pixel, float noise_offset, vec3 origin, vec3 normal, int offset)
{
	vec3 color = vec3(0.0);
	vec3 weight = vec3(1.0);
	const int bounces = 2;
	for(int i = 0; i < bounces; i++)
	{
		float pdf = 1.0;
		vec3 direction = diffuseRayGen(tex_bluenoise, pixel, noise_offset, i + offset * 2, normal, pdf);
		weight *= 1.0 / max(pdf, 0.0001) * max(0.0, dot(normal, direction));
	
		rayhit hit;
		traceRay(origin, direction, tex_world, tex_material, world_size, hit, time_ellapsed);
		normal = hit.normal;
		
		#define FAR 1e8
		
		if(hit.t > FAR)
		{
			color += weight * texture(tex_sky, normalize(hit.dir)).rgb;
			break;
		}
		
		material m = getWorldMaterial(tex_material, hit.voxel, hit.pos, time_ellapsed);
		
		vec3 incomingLight = vec3(0.0);
		
		// Sunlight
		rayhit hit2;
		if(!traceRayNoMirror(hit.pos, lightDir, tex_world, world_size, hit2))
			incomingLight += max(0.0, dot(lightDir, hit.normal)) * lightColor;
		
		if(i == bounces - 1)
		{
			int faceIndex = toFace(hit.normal);
			incomingLight += texelFetch(tex_lightmap, ivec3(hit.voxel.x, hit.voxel.y, hit.voxel.z + faceIndex * world_size.z), 0).rgb;
		}
		
		color += (m.albedo * incomingLight / PI + m.emmisive) * weight;
		weight *= m.albedo / PI;
		origin = hit.pos + normal * 0.01;
	}
	
	return color;
}
