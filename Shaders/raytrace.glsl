#include world.glsl

// Adapted from: https://www.reddit.com/r/opengl/comments/8ntzz5/fast_glsl_ray_box_intersection/
bool BBoxIntersect(const vec3 boxMin, const vec3 boxMax, vec3 o, vec3 invDir, out float t0, out float t1)
{
	vec3 tbot = invDir * (boxMin - o);
	vec3 ttop = invDir * (boxMax - o);
	vec3 tmin = min(ttop, tbot);
	vec3 tmax = max(ttop, tbot);
	vec2 t = max(tmin.xx, tmin.yz);
	t0 = max(t.x, t.y);
	t = min(tmax.xx, tmax.yz);
	t1 = min(t.x, t.y);
	return t1 > max(t0, 0.0);
}

// http://www.iquilezles.org/blog/?p=2411
float iSphere( in vec3 sc, in float sr, in vec3 ro, in vec3 rd )
{
    vec3 oc = ro - sc;
    float b = dot(rd, oc);
    float c = dot(oc, oc) - sr*sr;
    float t = b*b - c;
    if( t > 0.0) 
        t = -b - sqrt(t);
    return t;
}

bool isInWorld(ivec3 p, ivec3 size)
{
	return p.x >= 0 && p.y >= 0 && p.z >= 0
		&& p.x < size.x && p.y < size.y && p.z < size.z;
}

struct rayhit {
	vec3 normal;
	float t;
	ivec3 voxel;
	vec3 pos;
	vec3 dir;
};

bool traceRayR(vec3 o, vec3 d, usampler3D world, ivec3 size, out rayhit hit)
{
	hit.dir = d;
	hit.normal = vec3(0.0);
	hit.t = 0.0;
	float t0 = 0.0;
	float t1 = 0.0;
	/**/
	bool worldIntersect = BBoxIntersect(vec3(0.0), vec3(size), o, 1.0 / d, t0, t1);
	bool inWorld = (all(greaterThanEqual(o, vec3(0.0))) && all(lessThan(o, vec3(size))));
	worldIntersect = worldIntersect || inWorld;
	
	if(worldIntersect && !inWorld)
	{
		float t2,t3;
		if(BBoxIntersect(vec3(-1.0), vec3(size+1), o, 1.0 / d, t2, t3))
			hit.t = max(hit.t, t2+0.5/length(d));
	}
	
	if(!worldIntersect)
	{
		hit.t = 1e9;
		return false;
	}
	
	// Adapted (and butchered) from: http://www.cse.yorku.ca/~amana/research/grid.pdf
	vec3 p = o + d*hit.t;
	hit.voxel = ivec3(floor(p));
	vec3 fp = fract(p);
	
	ivec3 stepDir = ivec3(d.x > 0 ? 1 : -1, d.y > 0 ? 1 : -1, d.z > 0 ? 1 : -1);
	vec3 tDelta = 1.0 / abs(d);
	
	
	bool hadValid = false;
	float lastT = hit.t;
	int emptyCube = 1;
	int iters = 0;
	int maxIters = size.x + size.y + size.z + 20;
	while(hit.t < t1 && iters < maxIters)
	{
		//emptyCube = 1;
		vec3 tmax = vec3(hit.t);
		float mmin = 1e9;
		int moveAxis = 0;
		for(int i = 0; i < 3; i++)
		{
			if(d[i] > 0.0)
				tmax[i] += (emptyCube - fp[i]) / d[i];
			else
				tmax[i] += (fp[i] + emptyCube - 1) / -d[i];
			if(tmax[i] < mmin)
			{
				mmin = tmax[i];
				moveAxis = i;
			}
		}
		
		hit.t = tmax[moveAxis];
		p = o + d*hit.t;
		fp = fract(p);
		fp[moveAxis] = d[moveAxis] > 0 ? 0.0 : 1.0;
		
		int cvo = hit.voxel[moveAxis];
		hit.voxel = ivec3(floor(p));
		hit.voxel[moveAxis] = cvo + stepDir[moveAxis] * emptyCube;
		
		if(isInWorld(hit.voxel, size))
		{
			emptyCube = int(texelFetch(world, hit.voxel, 0).x);
			hadValid = true;
		}
		else
		{
			if(hadValid)
				break;
		}
		if(emptyCube == 0)
		{
			// We hit a opaque emptyCube
			// Get normal from the last move axis
			hit.normal[moveAxis] = -stepDir[moveAxis];
			hit.pos = p;
			return true;
		}
		iters++;
	}

	hit.t = 1e9;
	return false;
}

bool traceRayNoMirror(vec3 o, vec3 d, usampler3D world, ivec3 size, out rayhit hit)
{
	bool result = traceRayR(o, d, world, size, hit);
	vec3 sc = vec3(70.0);
	float sr = 1.0;
	float len = length(d);
	float sph = iSphere(sc, sr, o, d / len) / len;
	if(sph < hit.t && sph > 0.0)
	{
		hit.t = sph;
		hit.normal = normalize(o + d * sph - sc);
		hit.voxel = ivec3(-1);
		hit.pos = o + d * hit.t;
		return true;
	}
	return result;
}

bool traceRay(vec3 o, vec3 d, usampler3D world, usampler3D world_material, ivec3 size, out rayhit hit, float ellapsed_t)
{
	if(!traceRayNoMirror(o, d, world, size, hit))
		return false;
	material m = getWorldMaterial(world_material, hit.voxel, hit.pos, ellapsed_t);
	
	float depth = hit.t;
	
	int its = 1;
	
	while(m.mirror && its < 4)
	{
		d = reflect(d, hit.normal);
		traceRayNoMirror(hit.pos + d * 0.001, d, world, size, hit);
		m = getWorldMaterial(world_material, hit.voxel, hit.pos, ellapsed_t);
		depth += hit.t;
		its++;
	}
	
	hit.t = depth;
	
	return true;
}

vec4 GetBlueNoise(ivec2 p, int frame, sampler2D tex)
{
	const int texSizePow = 7;
	const int mask = (1 << texSizePow) - 1;
	ivec2 coords = p & mask;
	ivec2 tile = (p >> texSizePow) & 1;
	if((frame & 0x4) > 0)
		coords.x = mask - coords.x;
	if((frame & 0x8) > 0)
		coords.y = mask - coords.y;
	if((frame & 0x10) > 0)
		coords.x += 1 << (texSizePow - 1);
	if((frame & 0x20) > 0)
		coords.y += 1 << (texSizePow - 1);
	return texelFetch(tex, coords & ((1 << texSizePow) - 1), 0);
}
