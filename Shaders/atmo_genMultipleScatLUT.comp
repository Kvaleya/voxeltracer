#version 430

#include atmoScattering_antizem.glsl

layout(binding = 0) uniform sampler3D texLutScattering;

layout (binding = 0, rgba32f) uniform restrict writeonly image3D texMultipleScattering;
layout (binding = 1, rgba32f) uniform restrict readonly image3D texScatteringSumRead;
layout (binding = 2, rgba32f) uniform restrict writeonly image3D texScatteringSumWrite;

uniform ivec3 lutSize;

layout (local_size_x = 16, local_size_y = 16, local_size_z = 1) in;
void main()
{
	//if(gl_GlobalInvocationID.x >= lutSize.x || gl_GlobalInvocationID.y >= lutSize.y || gl_GlobalInvocationID.z >= lutSize.z)
	//	return;
	ivec3 global = ivec3(gl_GlobalInvocationID.xyz);
	
	vec3 tc = (vec3(global) + 0.5) / vec3(lutSize);
	vec3 viewParams = TcToViewParams(tc);
	
	vec3 p = ATMO_CENTER;
	p.y += atmo_planet_radius + viewParams.x;
	
	vec3 dir = vec3(0.0);
	dir.y = viewParams.y;
	dir.x = sqrt(1.0 - dir.y * dir.y);
	
	vec3 sunlight_dir = vec3(0.0);
	sunlight_dir.y = viewParams.z;
	sunlight_dir.x = sqrt(1.0 - sunlight_dir.y * sunlight_dir.y);
	
	vec3 accumR, accumM;
	MultipleScatteringPrecomp(p, dir, sunlight_dir, texLutScattering, accumR, accumM);
	
	vec4 finalScattering = vec4(accumR, accumM.x);
	imageStore(texMultipleScattering, global, finalScattering);
	
	vec4 sum = imageLoad(texScatteringSumRead, global);
	sum += finalScattering;
	imageStore(texScatteringSumWrite, global, sum);
}