#version 450 core

#include world.glsl

layout(binding = 0) uniform usampler3D texWorld;

layout(binding = 0, std430) restrict buffer bufferDispatch
{
    uint dispatch[];
};

layout(binding = 1, std430) restrict writeonly buffer bufferLightmapFaces
{
    uint lightmapFaces[];
};

uniform ivec3 worldSize;
uniform int wsizezpowz;

layout (local_size_x = 4, local_size_y = 4, local_size_z = 4) in;
void main(void)
{
	ivec3 pos = ivec3(gl_GlobalInvocationID.xyz);
	
	ivec3 voxel = ivec3(pos.x, pos.y, pos.z & ((1 << wsizezpowz) - 1));
	int faceIndex = (pos.z >> wsizezpowz);
	ivec3 faceDir = getFace(faceIndex);

	// Check face validity (is voxel air or is face obscured?)
	ivec3 neighbour = voxel + faceDir;
	if(!world_isCollision(texWorld, voxel, worldSize) || world_isCollision(texWorld, neighbour, worldSize))
		return;

	uint index = atomicAdd(dispatch[3], 1) - 1;
	atomicMax(dispatch[0], (index / 64) + 1);
	
	uint facedata = 0;
	facedata = facedata | ((voxel.x & 0xff) << 0);
	facedata = facedata | ((voxel.y & 0xff) << 8);
	facedata = facedata | ((voxel.z & 0xff) << 16);
	facedata = facedata | ((faceIndex & 0x7) << 24);
	
	lightmapFaces[index] = facedata;
}
