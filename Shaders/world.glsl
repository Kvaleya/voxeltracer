#ifndef PI
#define PI 3.1415926535897932384626433832795
#endif

struct material {
	vec3 albedoNonlinear;
	vec3 albedo;
	vec3 emmisive;
	bool mirror;
};

uniform vec3 lightDir;
//const vec3 lightColor = vec3(1.0, 0.8, 0.7) * 2.0;
uniform vec3 lightColor;

const vec3 material_f0 = vec3(0.1);
const float material_roughness = 0.2;
const float material_alpha = material_roughness * material_roughness;

bool world_isCollision(usampler3D world, ivec3 voxel, ivec3 wsize)
{
	if(voxel.x < 0 || voxel.y < 0 || voxel.z < 0 ||
		voxel.x >= wsize.x || voxel.y >= wsize.y || voxel.z >= wsize.z)
		return false;
	return texelFetch(world, voxel, 0).x == 0;
}

/*
// For a packed one bit per voxel world (todo?)
bool isCollision(ivec3 voxel, usampler3D world, ivec3 wsize)
{
	if(voxel.x < 0 || voxel.y < 0 || voxel.z < 0 ||
	voxel.x >= wsize.x || voxel.y >= wsize.y || voxel.z >= wsize.z)
		return false;
	uint data = texelFetch(world, voxel >> 1, 0);
	voxel = voxel & 1;
	int bit = voxel.x + voxel.y * 2 + voxel.z * 4;
	return (data & (1 << bit)) != 0;
}
*/

vec3 getSky(vec3 dir)
{
	dir = normalize(dir);
	vec3 col = mix(vec3(0.5, 0.4, 0.3), vec3(0.7, 0.8, 1.0), dir.y*0.5+0.5);
	col *= vec3(0.6, 0.7, 1.0);
	vec3 colsun = col + vec3(1.0, 0.5, 0.2) * pow(max(dot(normalize(vec3(1.0, -0.1, 0.4)), dir), 0.0), 4.0);
	col = colsun;
	col *= 0.25;
	return col;
}

// Smooth HSV to RGB conversion 
vec3 hsv2rgb_smooth( in vec3 c )
{
    vec3 rgb = clamp( abs(mod(c.x*6.0+vec3(0.0,4.0,2.0),6.0)-3.0)-1.0, 0.0, 1.0 );

	rgb = rgb*rgb*(3.0-2.0*rgb); // cubic smoothing	

	return c.z * mix( vec3(1.0), rgb, c.y);
}

layout(binding = 7, std430) restrict readonly buffer bufferPallete
{
    uint colorPallete[];
};

material getWorldMaterial(usampler3D texWorldMaterialFetch, ivec3 voxel, vec3 pos, float el)
{
	material m;
	m.albedoNonlinear = vec3(0.5);
	m.emmisive = vec3(0.0);
	m.mirror = false;
	int v = int(texelFetch(texWorldMaterialFetch, voxel, 0).x);
	/**/
	uint color = colorPallete[v];
	m.albedoNonlinear.r = float((color >> 16) & 0xff) / 255.0;
	m.albedoNonlinear.g = float((color >>  8) & 0xff) / 255.0;
	m.albedoNonlinear.b = float((color >>  0) & 0xff) / 255.0;
	
	m.albedo = pow(m.albedoNonlinear, vec3(2.2));
	
	int mask = 3;
	ivec3 lightVox = voxel >> 2;
	if((lightVox.x & mask) == 0 && (lightVox.y & mask) == 0 && (lightVox.z & mask) == 0)
	{
		m.emmisive = vec3(1.0, 0.5, 0.125) * 1.0;
	}
	
	//if(v + 1 == 45)
	//	m.mirror = true;
	
	/*/
	if(v == 1)
		m.emmisive = vec3(16.0, 8.0, 4.5) * 8.0;
	if(v == 2)
		m.emmisive = vec3(4.5, 12.0, 16.0);
	
	if(v == 3)
		m.albedo = vec3(1.0, 0.1, 0.1);
	if(v == 4)
		m.albedo = vec3(0.1, 1.0, 0.1);
	if(v == 5)
		m.albedo = vec3(0.1, 0.1, 1.0);
	if(v == 6 || voxel.x < 0)
		m.mirror = true;
	
	if(v == 255)
		m.emmisive = hsv2rgb_smooth(vec3(pos.z / 16.0 + el * 0.1, 1.0, 1.0)) * 32.0;
	m.emmisive *= 16.0;
	/**/
	
	if(voxel.x < 0)
		//m.emmisive = vec3(16.0, 8.0, 4.5) * 256.0;
		m.mirror = true;
	
	m.albedo = m.albedo * (1.0 - material_f0);
	
	return m;
}

ivec3 getFace(int i)
{
	int d = (i & 1) == 0 ? -1 : 1;
	ivec3 f = ivec3(0);
	f[i >> 1] = d;
	return f;
}

int toFace(vec3 normal)
{
	if(normal.x > 0.5)
		return 1;
	if(normal.y < -0.5)
		return 2;
	if(normal.y > 0.5)
		return 3;
	if(normal.z < -0.5)
		return 4;
	if(normal.z > 0.5)
		return 5;
	return 0;
}

ivec3 getFaceCoord(ivec3 voxel, int wsizez, int face)
{
	return ivec3(voxel.x, voxel.y, voxel.z + face * wsizez);
}

ivec3 getFaceCoord(ivec3 voxel, int wsizez, vec3 normal)
{
	return getFaceCoord(voxel, wsizez, toFace(normal));
}

vec3 mapVoxelTcToWorld(ivec3 voxel, int face, vec2 tc)
{
	vec3 p = vec3(voxel);
	if((face >> 1) == 0)
	{
		p.y += tc.x;
		p.z += tc.y;
	}
	if((face >> 1) == 1)
	{
		p.x += tc.x;
		p.z += tc.y;
	}
	if((face >> 1) == 2)
	{
		p.x += tc.x;
		p.y += tc.y;
	}
	
	if(face == 1)
		p.x += 1.0;
	if(face == 3)
		p.y += 1.0;
	if(face == 5)
		p.z += 1.0;
	
	return p;
}

vec2 mapVoxelWorldToTc(ivec3 voxel, int face, vec3 p)
{
	p = clamp(p, vec3(voxel), vec3(voxel + 0.999));
	p = fract(p);
	
	vec2 tc;
	
	if((face >> 1) == 0)
	{
		tc = p.yz;
	}
	if((face >> 1) == 1)
	{
		tc = p.xz;
	}
	if((face >> 1) == 2)
	{
		tc = p.xy;
	}
	
	return tc;
}

// https://gamedev.stackexchange.com/a/135189
vec2 toSpherical(in vec3 n)
{
    vec2 uv;

    uv.x = atan(-n.x, n.y);
    uv.x = (uv.x + PI / 2.0) / (PI * 2.0) + PI * (28.670 / 360.0);

    uv.y = acos(n.z) / PI;

    return uv;
}
// Uv range: [0, 1]
vec3 fromSpherical(in vec2 uv)
{
    float theta = 2.0 * PI * uv.x + - PI / 2.0;
    float phi = PI * uv.y;

    vec3 n;
    n.x = cos(theta) * sin(phi);
    n.y = sin(theta) * sin(phi);
    n.z = cos(phi);

    //n = normalize(n);
    return n;
}

uvec4 packPositionGbuffer(ivec3 voxel, int face, vec3 hitpos, float depth, vec3 view)
{
	uvec4 u = uvec4(0);
	u.x |= (voxel.x & 0xff) << 0;
	u.x |= (voxel.y & 0xff) << 8;
	u.x |= (voxel.z & 0xff) << 16;
	u.x |= (face & 0x7) << 24;
	// 5 bits free in x
	
	vec3 in_voxel = clamp(hitpos - voxel, 0.0, 1.0);
	ivec3 invox = clamp(ivec3(in_voxel * 32767), 0, 32767);
	
	// 2 bits free in y
	u.y |= (invox.x & 0x7fff) << 0;
	u.y |= (invox.y & 0x7fff) << 15;
	
	// 17 bits free in z
	u.z |= (invox.z & 0x7fff) << 0;
	
	// 0 bits free in w
	u.w = floatBitsToUint(depth);
	
	uvec2 s = uvec2(toSpherical(view) * ((1 << 12) - 1));
	
	u.z |= (s.x & 0xfff) << 15;
	u.z |= ((s.y >> 0) & 0x1f) << 27;
	
	u.y |= ((s.y >> 5) & 0x3) << 30;
	u.x |= ((s.y >> 7) & 0x1f) << 27;
	
	return u;
}

void unpackPositionGbuffer(uvec4 u, out ivec3 voxel, out int face, out vec3 hitpos, out float depth, out vec3 view)
{
	voxel.x = int((u.x >> 0) & 0xff);
	voxel.y = int((u.x >> 8) & 0xff);
	voxel.z = int((u.x >> 16) & 0xff);
	face = int((u.x >> 24) & 0x7);
	
	hitpos.x = float((u.y >> 0) & 0x7fff) / 32767.0;
	hitpos.y = float((u.y >> 15) & 0x7fff) / 32767.0;
	hitpos.z = float((u.z >> 0) & 0x7fff) / 32767.0;
	
	hitpos += vec3(voxel);
	
	depth = uintBitsToFloat(u.w);
	
	uvec2 si = uvec2(0);
	
	si.x = (u.z >> 15) & 0xfff;
	si.y |= ((u.z >> 27) & 0x1f) << 0;
	si.y |= ((u.y >> 30) & 0x3) << 5;
	si.y |= ((u.x >> 27) & 0x1f) << 7;
	
	view = fromSpherical(vec2(si) / ((1 << 12) - 1));
}

void unpackPositionGbuffer(uvec4 u, out ivec3 voxel, out int face, out vec3 hitpos, out float depth)
{
	vec3 view;
	unpackPositionGbuffer(u, voxel, face, hitpos, depth, view);
}

bool inrange(ivec3 a, ivec3 size)
{
	return a.x >= 0 && a.y >= 0 && a.z >= 0 && a.x < size.x && a.y < size.y && a.z < size.z;
}

bool isLightmapCollision(usampler3D world, ivec3 voxel, ivec3 wsize)
{
	return world_isCollision(world, voxel, wsize);
}

vec3 sampleLightmapSmooth(usampler3D tex_world, sampler3D tex_lightmap, ivec3 world_size, ivec3 voxel, int face, vec3 pos)
{
	vec3 in_voxel = clamp(pos - voxel, 0.0001, 0.9999);
	in_voxel[face >> 1] = 0.5;
	ivec3 facedir = getFace(face);
	ivec3 cornerdir = ivec3(0);
	int validdirs[2] = {0, 0};
	int dir_index = 0;
	// Test edges
	// o
	// xo
	for(int i = 0; i < 3; i++)
	{
		if(i == (face >> 1))
			continue;
		validdirs[dir_index] = i;
		dir_index++;
		ivec3 intdir = ivec3(0);
		int dir = in_voxel[i] > 0.5 ? 1 : -1;
		intdir[i] = dir;
		cornerdir[i] = dir;

		if(!isLightmapCollision(tex_world, voxel + intdir, world_size) || isLightmapCollision(tex_world, voxel + intdir + facedir, world_size))
		{
			in_voxel[i] = 0.5;
		}
	}
	
	// Test corner
	//  o
	// x
	
	if(!isLightmapCollision(tex_world, voxel + cornerdir, world_size) || isLightmapCollision(tex_world, voxel + cornerdir + facedir, world_size))
	{
		vec3 d = abs(in_voxel * 2.0 - 1.0);
		if(d[validdirs[0]] < d[validdirs[1]])
			in_voxel[validdirs[0]] = 0.5;
		else
			in_voxel[validdirs[1]] = 0.5;
	}
	
	vec3 tc = (ivec3(voxel.x, voxel.y, voxel.z + face * world_size.z) + in_voxel) / vec3(world_size.x, world_size.y, world_size.z * 6);
	return texture(tex_lightmap, tc).rgb;
}
