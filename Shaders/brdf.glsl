// Based on:
// https://schuttejoe.github.io/post/ggximportancesamplingpart1/

// Geometry term:
// https://twvideo01.ubm-us.net/o1/vault/gdc2017/Presentations/Hammon_Earl_PBR_Diffuse_Lighting.pdf
// slide ~80

#ifndef PI
#define PI 3.1415926535897932384626433832795
#endif

// Adapted from: // https://schuttejoe.github.io/post/ggximportancesamplingpart1/
vec3 FresnelSchlick(vec3 r0, float dotLightMicrofacet)
{
    return r0 + (1.0 - r0) * pow(1.0 - dotLightMicrofacet, 5.0);
}

float DistributionGGX(vec3 geometryNormal, vec3 microfacetNormal, float alpha)
{
	float d = dot(geometryNormal, microfacetNormal);
	float denom = d * d * (alpha * alpha - 1.0) + 1.0;
	return (alpha * alpha) / (PI * denom * denom);
}

float GeometrySmith(vec3 geometryNormal, vec3 view, vec3 light, float alpha)
{
	float dotgi = dot(geometryNormal, light);
	float dotgo = dot(geometryNormal, view);
	float a2 = alpha * alpha;
	float nom = 2 * dotgi * dotgo;
	float denom = dotgo * sqrt(a2 + (1.0 - a2) * dotgi * dotgi) + dotgi * sqrt(a2 + (1.0 - a2) * dotgo * dotgo);
	return nom / denom;
}

vec3 evalBrdf(vec3 normal, vec3 view, vec3 light, vec3 f0, float alpha)
{
	vec3 microfacet = normalize(view + light);
	vec3 f = FresnelSchlick(f0, dot(microfacet, light));
	float d = DistributionGGX(normal, microfacet, alpha);
	float g = GeometrySmith(normal, view, light, alpha);
	
	return f*d*g / (4.0 * dot(normal, view) * dot(normal, light)) * max(dot(normal, light), 0.0);
}

float evalBrdf(vec3 normal, vec3 view, vec3 light, float alpha)
{
	vec3 microfacet = normalize(view + light);
	float d = DistributionGGX(normal, microfacet, alpha);
	float g = GeometrySmith(normal, view, light, alpha);
	
	return d*g / (4.0 * dot(normal, view) * dot(normal, light)) * max(dot(normal, light), 0.0);
}

// https://schuttejoe.github.io/post/ggximportancesamplingpart2/
float SmithGGXMasking(vec3 normal, vec3 light, vec3 view, float a2)
{
	float dotNL = max(0.0, dot(normal, light));
	float dotNV = max(0.0, dot(normal, view));
	float denomC = sqrt(a2 + (1.0f - a2) * dotNV * dotNV) + dotNV;

	return 2.0 * dotNV / denomC;
}

vec3 sampleGGXVNDF(vec3 tangentSpaceView, float alpha, vec2 u)
{
	vec3 V = normalize(vec3(alpha * tangentSpaceView.x, tangentSpaceView.y, alpha * tangentSpaceView.z));
	vec3 T1 = (V.z < 0.9999) ? normalize(cross(V, vec3(0,0,1))) : vec3(1,0,0);
	vec3 T2 = cross(T1, V);
	float a = 1.0 / (1.0 + V.z);
	float r = sqrt(u.x);
	float phi = (u.y<a) ? u.y/a * PI : PI + (u.y-a)/(1.0-a) * PI;
	float P1 = r*cos(phi);
	float P2 = r*sin(phi)*((u.y<a) ? 1.0 : V.z);
	vec3 N = P1*T1 + P2*T2 + sqrt(max(0.0, 1.0 - P1*P1 - P2*P2))*V;
	N = normalize(vec3(alpha*N.x, max(0.0, N.y), alpha*N.z));
	return N;
}

float getGGXPdf(vec3 normal, vec3 view, vec3 light, vec3 microfacet, float alpha)
{
	float nom = SmithGGXMasking(normal, light, view, alpha * alpha) * DistributionGGX(normal, microfacet, alpha);
	return nom / (4.0 * max(0.0, dot(view, normal)));
}
