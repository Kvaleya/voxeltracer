#version 430

layout (binding = 0, rgba16f) uniform restrict writeonly image2D texOutput;

#include raytrace.glsl
#include world.glsl
#include diffusegi.glsl

uniform float noiseOffset;
uniform float ellapsed;
uniform vec3 ray00;
uniform vec3 ray01;
uniform vec3 ray10;
uniform vec3 ray11;
uniform vec3 cameraPos;
uniform ivec3 worldSize;
uniform vec2 sizercp;
uniform vec2 finalSizercp;

uniform int resshift;

layout(binding = 0) uniform usampler3D texWorld;
layout(binding = 3) uniform usampler3D texWorldMat;
layout(binding = 1) uniform sampler2D texGbufferNormal;
layout(binding = 2) uniform usampler2D texGbufferPos;
layout(binding = 4) uniform sampler2D texBlueNoise;
layout(binding = 5) uniform sampler3D texLightmap;
layout(binding = 6) uniform samplerCube texSky;

vec3 GetRay(vec2 tc)
{
	return mix(
		mix(ray01, ray11, tc.x),
		mix(ray00, ray10, tc.x),
		1.0 - tc.y);
}

vec2 pixelToRealScreenpos(ivec2 pix)
{
	return ((pix << resshift) + 0.5) * finalSizercp;
}

layout (local_size_x = 8, local_size_y = 8, local_size_z = 1) in;
void main()
{
	ivec2 pixelPos = ivec2(gl_GlobalInvocationID.xy);
	vec2 screenPos = pixelToRealScreenpos(pixelPos);
	vec3 direction = GetRay(screenPos);
	
	vec3 pos;
	float depth;
	ivec3 voxel;
	int face;
	unpackPositionGbuffer(texelFetch(texGbufferPos, pixelPos << resshift, 0), voxel, face, pos, depth);
	vec3 normal = normalize(texelFetch(texGbufferNormal, pixelPos << resshift, 0).xyz * 2.0 - 1.0);
	vec3 origin = pos + normal * 0.001;
	
	vec3 diffuse = vec3(0.0);
	
	const int samples = 1;
	for(int j = 0; j < samples; j++)
	{
		vec3 color = pathTraceGi(texWorld, texWorldMat, texLightmap, texSky, worldSize, ellapsed, texBlueNoise, pixelPos, noiseOffset, pos, normal, j);
		//vec3 color = traceLightmap(origin, normal, float(j));
		diffuse += color / float(samples);
	}
	//diffuse = texelFetch(texLightmap, ivec3(voxel.x, voxel.y, voxel.z + face * worldSize.z), 0).rgb;
	
	imageStore(texOutput, pixelPos, vec4(diffuse, 0.0));
}
