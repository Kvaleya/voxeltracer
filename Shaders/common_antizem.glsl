float GetDither2(ivec2 p)
{
	float d = 0.0;
	
	if((p.x & 1) != (p.y & 1))
		d += 2.0;
	if((p.y & 1) == 1)
		d += 1.0;
	
	d *= 0.25;
	
	return d;
}

float GetDither4(ivec2 p)
{
	float d = GetDither2(p);
	d = d * 0.25 + GetDither2(p >> 1);
	return d;
}

float GetBlueNoise(ivec2 p, int frame, sampler2D tex)
{
	const int texSizePow = 7;
	const int mask = (1 << texSizePow) - 1;
	ivec2 coords = p & mask;
	ivec2 tile = (p >> texSizePow) & 1;
	int component = (tile.x + tile.y * 2 + frame) & 0x3;
	if((frame & 0x4) > 0)
		coords.x = mask - coords.x;
	if((frame & 0x8) > 0)
		coords.y = mask - coords.y;
	if((frame & 0x10) > 0)
		coords.x += 1 << (texSizePow - 1);
	if((frame & 0x20) > 0)
		coords.y += 1 << (texSizePow - 1);
	return texelFetch(tex, coords & ((1 << texSizePow) - 1), 0)[component];
}

float maxcomp(vec2 v)
{
	return max(v.x, v.y);
}

float maxcomp(vec3 v)
{
	return max(v.x, max(v.y, v.z));
}

float maxcomp(vec4 v)
{
	return max(v.x, max(v.y, max(v.z, v.w)));
}

float mincomp(vec2 v)
{
	return min(v.x, v.y);
}

float mincomp(vec3 v)
{
	return min(v.x, min(v.y, v.z));
}

float mincomp(vec4 v)
{
	return min(v.x, min(v.y, min(v.z, v.w)));
}

vec2 pow2(vec2 v, float p)
{
	return vec2(pow(v.x, p), pow(v.y, p));
}

vec3 pow3(vec3 v, float p)
{
	return vec3(pow(v.x, p), pow(v.y, p), pow(v.z, p));
}

// https://stackoverflow.com/questions/39393560/glsl-memorybarriershared-usefulness
#define MEMORY_BARRIER_GROUPSHARED memoryBarrierShared(); barrier();

#define DEPTH_NEAR 100000000000.0
#define DEPTH_FAR 0.0

#define DIST_HORIZON 10000000.0

#define KV_PI 3.1415926535897932384626433832795

// On Nvidia Pascal, normalizing a zero vector results in NaNs
// On AMD GCN 1.0 this results in a zero vector, as does this function
// Only use this normalize variant when completely necesary
vec3 safeNormalize(vec3 v)
{
	float l = dot(v, v);
	return l > 0 ? v * inversesqrt(l) : v;
}

vec3 CubeTexCoordToVector(vec2 tc, int z)
{	
	if(z == 0)
		return vec3(1.0, -tc.y, -tc.x);
	if(z == 1)
		return vec3(-1.0, -tc.y, tc.x);
	if(z == 2)
		return vec3(tc.x, 1.0, tc.y);
	if(z == 3)
		return vec3(tc.x, -1.0, -tc.y);
	if(z == 4)
		return vec3(tc.x, -tc.y, 1.0);
	return vec3(-tc.x, -tc.y, -1.0);
}

bool hasnan(vec2 v)
{
	return isnan(v.x) || isnan(v.y);
}

bool hasnan(vec3 v)
{
	return isnan(v.x) || isnan(v.y) || isnan(v.z);
}

bool hasnan(vec4 v)
{
	return isnan(v.x) || isnan(v.y) || isnan(v.z) || isnan(v.w);
}

#define saturate(v) clamp(v, 0.0, 1.0)

float SampleVec(vec4 v, float f)
{
	int i = int(f);
	return mix(v[i], v[i+1], fract(f));
}

float SampleVec(vec3 v, float f)
{
	int i = int(f);
	return mix(v[i], v[i+1], fract(f));
}

float SampleVec(vec2 v, float f)
{
	int i = int(f);
	return mix(v[i], v[i+1], fract(f));
}

float ClSphere(vec3 p, vec3 c, float r)
{
	float len = length(p - c) / r;
	return clamp((1.0 - len) * 4.0, 0.0, 1.0);
}

float Remap(float value, float oldmin, float oldmax, float newmin, float newmax)
{
	return newmin + (value - oldmin) / (oldmax - oldmin) * (newmax - newmin);
}

float RemapSat(float value, float oldmin, float oldmax, float newmin, float newmax)
{
	return newmin + saturate((value - oldmin) / (oldmax - oldmin)) * (newmax - newmin);
}

float Gradient(float val, float low, float high)
{
	return saturate((val - low) / (high - low));
}

float GradientNoSat(float val, float low, float high)
{
	return ((val - low) / (high - low));
}
