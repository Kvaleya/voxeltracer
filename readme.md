
![](voxeltracer_avatar.jpg)

# Voxel tracer

Ray tracing voxels on the GPU.

Uses beautiful Monument Valley like demo maps from MagicaVoxel and https://github.com/ephtracy/voxel-model/tree/master/vox/monument

This project was mostly a prototyping playground.

## Running the project

The compiled executable assumes that it is run next to the ".sln", so set your working directory accordingly.

Project should run on Windows 10 with both AMD and Nvidia cards (tested GTX 1060 and R9 270X).

## Controls:

- WASD + mouse: FPS style movement
	- LSHIFT for faster movement
	- LALT for slower
- SPACE: lock/unlock mouse
- K: cycle quality presets (default is best, see console output)
- L: cycle levels
- I/O: move sun
- C: switch to "game mode"
	- you can run around the map
	- SPACE is jump

## Inner workings

- world is represented by two R8UI 3D textures:
	- collision texture, where 0 = collision, higher number is radius of the largest empty cube centered on the voxel (1 = 1x1x1, 2 = 3x3x3, 3 = 5x5x5...)
	- material texture with indices into a material palette
- ray tracing traverses the collision texture voxel by voxel and uses the empty cube radius to skip empty space, see raytrace.glsl for the tracing function

## Dynamic diffuse lightmap

- each exposed voxel face has 1 texel "lightmap"
- one ray is traced per lightmap per frame
	- exponencial accumulation
- diffuse GI tracing then only traces two bounces
	- second bounce also includes light from lightmap
	- same is used when tracing rays for lightmap itself
	- theoretically infinite bounce GI
		- about equivalent to about 8 bounces in practice
- also reduces noise a bit

## Other rendering features

- raytraced specular reflections
	- shadows in reflections are raytraced, diffuse illumination samples lightmap
- hard raytraced shadows
- atmospheric scattering
	- prerendered into a small cubemap that is sampled by diffuse and specular lighting
- SMAA used for antialiasing
- auto exposure
- live shader reloading (through my GlobCore library)

## Images

![](demo1.jpg)

![](demo2.jpg)

![](demo3.jpg)
